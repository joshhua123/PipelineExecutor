﻿//! A crate with utilities to get and set the system's host name.
//!
//! ## Examples
 
use std::ffi::OsStr;
use std::ffi::OsString;
use std::io; 

/// Return the system hostname.
///
/// ## Example
///
/// ```rust
/// # use std::io;
/// # fn try_main() -> io::Result<()> {
/// let name = hostname::get()?;
/// # Ok(())
/// # }
/// # fn main() {
/// #    try_main().unwrap();
/// # }
/// ```
///
/// ## Errors
///
/// If this function encounters any form of error, an error
/// variant will be returned; in practice it is rare to be happen.
pub fn get() -> io::Result<OsString> {    
    Ok(OsString::from("localhost"))
}

/// Set the system hostname.
///
/// This function is available only with `set` feature enabled (**disabled** by
/// default).
 
/// ## Errors
///
/// In order to set new hostname, caller might need
/// to have the corresponding privilege
/// (`CAP_SYS_ADMIN` capability for Linux, administrator privileges for Windows,
/// and so on).\
/// An error variant will be returned if this function
/// will encounter a permission error or any other form of error.
///
/// ## Compatibility
///
/// * Will fail with a linkage error for Android API < 23 (see [#9](https://github.com/svartalf/hostname/issues/9#issuecomment-563991112))
pub fn set<T>(_hostname: T) -> io::Result<()>
    where
        T: AsRef<OsStr>,
{ 
    Ok(())
}
