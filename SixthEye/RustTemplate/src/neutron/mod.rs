use std::ffi::CString;
use std::os::raw::c_char; 

fn store_string_on_heap(string_to_store: &'static str) -> *mut c_char {
    return CString::new(string_to_store).unwrap().into_raw();
}

extern fn free_string(ptr: *mut c_char) {
    unsafe {
        let _ = CString::from_raw(ptr); 
    }
} 

extern "C" { 
    fn Publish(pointer: *const c_char, length: usize);
}

/*
pub fn return_result(json: &'static str) {
    unsafe{ 
        let heap = store_string_on_heap(json); 
        Result(heap, json.len());
        free_string(heap);
    }
}
*/
