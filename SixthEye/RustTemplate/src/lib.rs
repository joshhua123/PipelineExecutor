mod neutron;

#[macro_use]
extern crate json;

#[no_mangle]
pub async extern "C" fn run() { 
    json::stringify(object!{ 
        name: "Maciej",
        age: 30
    });
}
#[no_mangle]
pub extern "C" fn init() {   

}

#[no_mangle]
pub extern "C" fn exit() {   

}