using System.IO;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using SixthEye.Infrastructure.Application;
using SixthEye.Infrastructure;
using SixthEye.Infrastructure.Executor;
using SixthEye.Infrastructure.Pipeline;

namespace PipelineService.Test;

[TestClass]
public class Tests
{
    [TestInitialize]
    public void Setup()
    {
    }

    [TestMethod]
    public void StatePipeline()
    {
        var pipeline1 = new StatePipeline();

        var pipeline2 = new StatePipeline();
        //pipeline2.Then((value) => Console.WriteLine("PABLO PABLO"), new StageConfig{Identifier = "test"});

        pipeline1.Feed(new State(), new StageConfig { Identifier = "test" });

        Thread.Sleep(1000);
    }

    public async Task Wasm()
    {
        var s =
            "{\"variables\":{\"term\":\"australia\",\"search\":\"{\\\"id\\\":0,\\\"domain\\\":\\\"nypost.com\\\",\\\"title\\\":\\\"Surfer dies after shark attack off Australia’s east coast\\\",\\\"url\\\":\\\"https://nypost.com/2021/09/05/surfer-dies-after-shark-attack-off-australias-east-coast/\\\",\\\"search_source\\\":0,\\\"publicated\\\":\\\"2021-09-05T12:28:12Z\\\",\\\"found\\\":\\\"2021-09-05T21:00:15.4218768Z\\\",\\\"summary\\\":null,\\\"content\\\":null,\\\"scraped\\\":null,\\\"SubscriptionId\\\":0}\",\"id\":\"0\",\"domain\":\"nypost.com\",\"title\":\"Surfer dies after shark attack off Australia’s east coast\",\"url\":\"https://nypost.com/2021/09/05/surfer-dies-after-shark-attack-off-australias-east-coast/\",\"search_source\":\"0\",\"publicated\":\"2021-09-05T12:28:12Z\",\"found\":\"2021-09-05T21:00:15.4218768Z\",\"SubscriptionId\":\"0\"},\"_conflicts\":[]}";

        var asm = await Services.ObjectStorage.Get("PipelineWASM.wasm") as MemoryStream;
 
        var x = JsonConvert.DeserializeObject<State>(s);


        //var exe = new WasmtimeExecutor(asm.GetBuffer(), "pipeline");

        //exe.Build(asm(), "greet");

        //await exe.Run(x);
    }

    [TestMethod]
    public void Actor()
    {
        var s =
            "{\"variables\":{\"term\":\"australia\",\"search\":\"{\\\"id\\\":0,\\\"domain\\\":\\\"nypost.com\\\",\\\"title\\\":\\\"Surfer dies after shark attack off Australia’s east coast\\\",\\\"url\\\":\\\"https://nypost.com/2021/09/05/surfer-dies-after-shark-attack-off-australias-east-coast/\\\",\\\"search_source\\\":0,\\\"publicated\\\":\\\"2021-09-05T12:28:12Z\\\",\\\"found\\\":\\\"2021-09-05T21:00:15.4218768Z\\\",\\\"summary\\\":null,\\\"content\\\":null,\\\"scraped\\\":null,\\\"SubscriptionId\\\":0}\",\"id\":\"0\",\"domain\":\"nypost.com\",\"title\":\"Surfer dies after shark attack off Australia’s east coast\",\"url\":\"https://nypost.com/2021/09/05/surfer-dies-after-shark-attack-off-australias-east-coast/\",\"search_source\":\"0\",\"publicated\":\"2021-09-05T12:28:12Z\",\"found\":\"2021-09-05T21:00:15.4218768Z\",\"SubscriptionId\":\"0\"},\"_conflicts\":[]}";

        var x = JsonConvert.DeserializeObject<State>(s);

        return;
    }
}