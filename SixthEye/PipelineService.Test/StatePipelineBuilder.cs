using System;
using System.Collections.Generic;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting; 
using SixthEye.Infrastructure.Executor;
using SixthEye.Infrastructure.Pipeline;
using SixthEye.Infrastructure.Pipeline.Source;
using SixthEye.Infrastructure.Pipeline.Target;

namespace PipelineService.Test;

[TestClass]
public class StatePipelineBuilder
{
    private void TestExecutor(IExecutor executor)
    {
        var pipeline = new StatePipeline();

        var source = new InMemorySource("test", nameof(StatePipelineBuilder));
        var target = new InMemoryTarget("test", nameof(StatePipelineBuilder));
 
        pipeline.Subscribe(source, null);
        pipeline.Then(executor, null);
        pipeline.Publish(target, null);
        
        Thread.Sleep(TimeSpan.FromSeconds(1));
        
        pipeline.CancellationTokenSource.Cancel();
        Assert.AreNotSame(0, target.Counters().Processed);
        Assert.AreNotSame(0, target.Counters().Peek.Count);
    }
 
    [TestMethod]
    public void Build()
    {
        //var executor = new Utilities();
        var executorParams = new Dictionary<string, string> {{ "on", "value"}, {"operation", "isEmpty"}, { "value", "false"}};
        //executor.Init("filter", executorParams);
        
       //TestExecutor(executor);
    }  
}