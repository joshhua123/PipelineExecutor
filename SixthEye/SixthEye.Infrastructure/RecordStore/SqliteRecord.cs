using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Data.Sqlite;
using SixthEye.Infrastructure.Application;
using SixthEye.Infrastructure.Pipeline;

namespace SixthEye.Infrastructure.RecordStore.Impl;

/// <summary>
/// Singleton
/// </summary>
public class SqliteRecord : IRecordStore
{
    private readonly SqliteConnection _connection; 

    private HashSet<string> _knownPartitions = new ();

    public SqliteRecord()
    {
        _connection = ConnectionFactory.GetConnection("records.db");
        
        using var command = _connection.CreateCommand(); 
        command.CommandText = "PRAGMA journal_mode=WAL";
        command.ExecuteNonQuery();

        List().GetAwaiter().GetResult();
    }

    private SqliteConnection GetConnection() {
        return ConnectionFactory.GetConnection("records.db");
    }


    private async Task CreateTableIfNotExists(string partition, SqliteConnection connection, string[] selector) {
        if (!_knownPartitions.Contains(partition)) { 
            await using var cmd = connection.CreateCommand();
            var columns = string.Join(",", selector.Select(x => $"{x} varchar"));
            cmd.CommandText = $"CREATE TABLE IF NOT EXISTS {partition} ({columns})"; 
            cmd.ExecuteNonQuery();
            _knownPartitions.Add(partition);
        }
    }
    
    public async Task<IEnumerable<IDictionary<string, string>>> Get(string partition)
    {
        await using var connection = GetConnection();
        await using  var command = connection.CreateCommand();
        command.CommandText = $"SELECT * FROM {partition}";

        await using var reader = await command.ExecuteReaderAsync(); 
        var result = new List<IDictionary<string, string>>();

        while (await reader.ReadAsync()) { 
            var row = new Dictionary<string, string>(); 
            for (var i = 0; i < reader.FieldCount; ++i) {
                row.Add(reader.GetName(i), reader.IsDBNull(i) ? string.Empty : reader.GetString(i));
            }
            result.Add(row);
        }

        return result;
    }

    public IEnumerable<IDictionary<string, string>> Iterate(string partition)
    {
        using var connection = GetConnection();
        using  var command = connection.CreateCommand();
        command.CommandText = $"SELECT * FROM {partition}";

        using var reader = command.ExecuteReader();   
        while (reader.Read()) { 
            var row = new Dictionary<string, string>(); 
            for (var i = 0; i < reader.FieldCount; ++i) {
                row.Add(reader.GetName(i), reader.IsDBNull(i) ? string.Empty : reader.GetString(i));
            }
            yield return row;
        }
    }

    public async Task<bool> Contains(string key, string value, string partition)
    {
        if (!_knownPartitions.Contains(partition))
            return false;
        
        await using var connection = GetConnection(); 
        await using var command = connection.CreateCommand();
        command.CommandText = $"SELECT * FROM {partition} WHERE {key} = $value LIMIT 1";
        command.Parameters.AddWithValue("value", value); 
        return await command.ExecuteNonQueryAsync() != -1;
    }

    public async Task<IEnumerable<string>> List()
    { 
        await using var connection = GetConnection(); 
        await using var command = connection.CreateCommand();
        command.CommandText = $"SELECT name FROM sqlite_master WHERE type ='table' AND name NOT LIKE 'sqlite_%';";
        await using var reader = await command.ExecuteReaderAsync(); 
        var result = new List<string>();

        while (await reader.ReadAsync()) { 
            result.Add(reader.GetString(0));
        }

        _knownPartitions = new HashSet<string>(result);
        
        return result;
    }

    public async Task Put(State state, string partition, string[] selector)
    {
        await using var connection = GetConnection();
        await CreateTableIfNotExists(partition, connection, selector);
        var keys = string.Join(",", selector);
        var values = string.Join(",", selector.Select(x => "$" + x));
        
        await using var command = connection.CreateCommand();
        command.CommandText = $"INSERT INTO {partition} ({keys}) VALUES ({values})";
        // TODO UPDATE ON (dedupe key)

        foreach (var key in selector) {
            state.variables.TryGetValue(key, out var value);
            command.Parameters.AddWithValue(key, value ?? string.Empty); 
        } 
        
        await command.ExecuteNonQueryAsync(); 
    }
}