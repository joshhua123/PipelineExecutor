using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Npgsql;
using SixthEye.Infrastructure.Application;

namespace SixthEye.Infrastructure.RecordStore.Impl;

public class NpgsqlRecord : IRecordStore { 

    private HashSet<string> _knownPartitions;
    private string _connString = Environment.GetEnvironmentVariable("RECORD_POSTGRE_CONNECTION") ??
                                         $"Host={Services.ServerHost};Username=klisadar;Password=klisadar;Database=postgres;";
  
    private async Task<NpgsqlConnection> GetConnection(bool multiplexing = true)
    { 
        var builder = new NpgsqlConnectionStringBuilder(_connString) {
            MaxPoolSize = 50,
            Multiplexing = multiplexing,
            Timeout = 50
        };
        _connString = builder.ToString(); 

        var connection = new NpgsqlConnection(_connString); 
        await connection.OpenAsync();
        return connection;
    }
    
    
    public async Task<IEnumerable<IDictionary<string, string>>> Get(string partition)
    {
        await using var connection = await GetConnection();
        await using  var command = connection.CreateCommand();
        command.CommandText = $"SELECT * FROM {partition}";

        await using var reader = await command.ExecuteReaderAsync(); 
        var result = new List<IDictionary<string, string>>();

        while (await reader.ReadAsync()) { 
            var row = new Dictionary<string, string>(); 
            for (var i = 0; i < reader.FieldCount; ++i) {
                row.Add(reader.GetName(i), reader.IsDBNull(i) ? string.Empty : reader.GetString(i));
            }
            result.Add(row);
        }

        return result;
    }

    public IEnumerable<IDictionary<string, string>> Iterate(string partition)
    {
        using var connection = GetConnection(false).GetAwaiter().GetResult();  
        using  var command = connection.CreateCommand();
        command.CommandText = $"SELECT * FROM {partition}";

        using var reader = command.ExecuteReader();

        while (reader.Read()) { 
            // TODO Is there an opportunity here to reuse the dictionary? 
            var row = new Dictionary<string, string>(reader.FieldCount); 
            for (var i = 0; i < reader.FieldCount; ++i) {
                row.Add(reader.GetName(i), reader.IsDBNull(i) ? string.Empty : reader.GetString(i));
            }
            yield return row;
        }
    }

    public async Task Put(State state, string partition, string[] selector)
    {        
        // todo A mechanism to handle selector changes and table schema mismatch 
        await using var connection = await GetConnection();
        _knownPartitions ??= new HashSet<string>(await List());
        if (!_knownPartitions.Contains(partition)) {
            // Since this command creates a table, we need to perform it without multiplexing
            await using var conn = await GetConnection(false);
            await using var cmd = conn.CreateCommand();
            lock (_knownPartitions) { 
                var columns = string.Join(",", selector.Select(x => $"{x} varchar"));
                cmd.CommandText = $"CREATE TABLE IF NOT EXISTS {partition} ({columns})";
                cmd.ExecuteNonQuery();
                _knownPartitions.Add(partition);
            }
        }

        var keys = string.Join(",", selector);
        var values = string.Join(",", selector.Select(x => ":" + x));
        
        await using var command = connection.CreateCommand();
        command.CommandText = $"INSERT INTO {partition} ({keys}) VALUES ( {values} )";
        // TODO UPDATE ON (dedupe key)

        foreach (var key in selector) {
            state.variables.TryGetValue(key, out var value);
            command.Parameters.AddWithValue(key, value ?? string.Empty); 
        } 
        
        await command.ExecuteNonQueryAsync(); 
    }

    public async Task<bool> Contains(string key, string value, string partition)
    {
        _knownPartitions ??= new HashSet<string>(await List());
        if (!_knownPartitions.Contains(partition))
            return false;
        
        await using var connection = await GetConnection();
        await using var command = connection.CreateCommand();
        command.CommandText = $"SELECT 1 FROM {partition} WHERE {key} = :value LIMIT 1";
        command.Parameters.AddWithValue("value", value); 
        return (await command.ExecuteScalarAsync() as int? ?? 0) == 1; 
    }

    public async Task<IEnumerable<string>> List() {
        await using var connection = await GetConnection();
        await using var command = connection.CreateCommand();
        command.CommandText = "select table_name from information_schema.tables where table_schema = 'public'";
        await using var reader = await command.ExecuteReaderAsync(); 
        var result = new List<string>();

        while (await reader.ReadAsync()) { 
            result.Add(reader.GetString(0));
        } 
        
        return result;
    }
}