﻿using System;
using System.Diagnostics.CodeAnalysis;

namespace SixthEye.Infrastructure.RemoteObject.Attributes
{

    public enum ScanOperations
    {
        IsNull,
        False,
        True,
        IsNullOrWhitespace
    }
    
   
    /// <summary>
    /// When performing a scan, this attribute is needed on columns we want to filter by
    /// predicate must be predefined.
    /// </summary>
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property)]
    public class Scan : System.Attribute
    {
        public ScanOperations Operation;
        public Scan(ScanOperations operation)
        {
            Operation = operation;
        }  
        
        public static string ExecuteOperation(object value, ScanOperations operation) 
        { 
            switch (operation)
            {
                case ScanOperations.IsNull:
                    return value == null ? "1" : "0";
                case ScanOperations.True:
                    return value as bool? == true ? "1" : "0";
                case ScanOperations.False:
                    return value as bool? == false ? "1" : "0";
                case ScanOperations.IsNullOrWhitespace:
                    return string.IsNullOrWhiteSpace(value as string) ? "1" : "0";
                
                default:
                    return "0";
            } 
        }
              
    }
}