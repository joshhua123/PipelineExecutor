﻿using System;

namespace SixthEye.Infrastructure.RemoteObject.Attributes
{
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property)]
    public class Length : System.Attribute
    {
        public int value;
        
        public Length(int value)
        {
            this.value = value;
        }   
    }
}