﻿using System;
using System.Reactive.Disposables;
using Loggly;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace SixthEye.Infrastructure
{
    public class LogglyAdapter : ILogger
    {
        private readonly LogglyClient instance;

        public LogglyAdapter()
        {
            instance = new LogglyClient(); 
        }
        
        public void Log<TState>(LogLevel logLevel, EventId eventId, TState state, Exception exception, Func<TState, Exception, string> formatter)
        {
            instance.Log(new LogglyEvent(new MessageData
            {
                {"logLevel", logLevel.ToString("G")},
                {"eventId", JsonConvert.SerializeObject(eventId) },
                {"state",  state != null ? JsonConvert.SerializeObject(state) : string.Empty},
                {"exception", exception != null ? JsonConvert.SerializeObject(exception) : string.Empty},
                {"formatted", formatter != null ? formatter(state, exception) : string.Empty} 
            }));
        }

        public bool IsEnabled(LogLevel logLevel)
        {
            return true;
        }

        public IDisposable BeginScope<TState>(TState state)
        {
            return new BooleanDisposable();
        }
    }
}