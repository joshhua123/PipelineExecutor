using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks; 
using AngleSharp.Dom; 
using SixthEye.Infrastructure.Application;
using Sentry;
using SixthEye.Infrastructure;
using SixthEye.Infrastructure.Pipeline;

namespace PipelineService.Plugins
{
    public class WebRequest : IPlugin
    {
        private readonly StageReporting _report = new(); 
        private readonly HttpClient _client = new();
        private enum FunctionEnum {
            Get
        } 
        private FunctionEnum _functionEnum;

        private string _on;
        
        public async Task<State[]> Run(State state)
        {
            return _functionEnum switch {
                FunctionEnum.Get => await Get(state),
                _ => null
            };
        }

        public void Init(string function, IDictionary<string, string> parameters)
        {  
            _functionEnum = function switch {
                 "get" => FunctionEnum.Get 
            };
             
            _ = _functionEnum switch {
                FunctionEnum.Get => IPlugin.ValidateParameters(new[] { "on" }, parameters), 
            };

            parameters.TryGetValue("on", out _on);
        }

        public IDictionary<string, IEnumerable<string>> Functions() {
            return new Dictionary<string, IEnumerable<string>>(){{"get", new [] {"on"}}};
        }

        public async Task<State[]> Get(State state)
        {
            if (!state.variables.TryGetValue(_on, out var url)) {
                _report.Dropped++;
                return Array.Empty<State>();
            }

            try
            { 
                state.variables["get"] = await _client.GetStringAsync(Url.Create(url));
                _report.Processed++; 
                _report.AppendPeek(state);
                return new[] { state };
            }
            catch (Exception ex)
            {
                SentrySdk.CaptureException(ex);
                _report.Fatal++;
                _report.AppendPeek(state);
                return Array.Empty<State>();
            }
        }

        public object SelfReport()
        {
            return new { nameof = nameof(WebRequest) };
        }

        public StageReporting Counters()
        {
            return _report;
        }

        public void Dispose()
        {
            _client?.Dispose();
        }
    }
}