﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace SixthEye.Infrastructure.Metrics
{
    public class NullMetrics : IMetrics
    { 
        public void Report(string source, string measurement, object fields) { }
        public Task<Metric> Get(string stage)
        {
            return Task.FromResult(new Metric());
        }

        public void Dispose() { }
    }
}