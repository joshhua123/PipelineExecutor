﻿using System;
using System.IO;
using System.Threading.Tasks;
using InfluxDB.Client;
using InfluxDB.Client.Api.Domain;
using InfluxDB.Client.Writes;
using Microsoft.EntityFrameworkCore.ChangeTracking.Internal;
using Sentry;

namespace SixthEye.Infrastructure.Metrics
{
    public class InfluxMetrics : IMetrics
    {
        private readonly InfluxDBClient _client;
        private readonly string _bucket;
        private readonly string _organisation; 
        private readonly WriteApi _write;
        private readonly QueryApi _query;
         

        public InfluxMetrics(string hostname, string bucket, string token, string org = "my-org")
        {
            _bucket = bucket;
            _organisation = org;
            _client = InfluxDBClientFactory.Create(hostname, token.ToCharArray()); 
            _write = _client.GetWriteApi();
            _query = _client.GetQueryApi();

        } 

        public void Report(string source, string measurement, object fields)
        {
            try {
                var reportType = fields.GetType();

                var point = PointData
                    .Measurement(measurement)
                    .Tag("Host", Environment.MachineName)
                    .Tag("source", source)
                    .Tag("Assembly", Path.GetFileName(reportType.Assembly.Location))
                    .Timestamp(DateTime.UtcNow, WritePrecision.S);

                foreach (var properties in reportType.GetProperties())
                {
                    var value = properties.GetValue(fields);

                    if (value == null)
                        continue;

                    point = value switch
                    {
                        string s => point.Field(properties.Name, s ?? string.Empty),
                        int _ => point.Field(properties.Name, value as int? ?? default),
                        float _ => point.Field(properties.Name, value as float? ?? default),
                        long _ => point.Field(properties.Name, value as long? ?? default),
                        decimal _ => point.Field(properties.Name, value as decimal? ?? default),
                        double _ => point.Field(properties.Name, value as double? ?? default),
                        _ => point.Field(properties.Name, value.ToString())
                    };
                }

                _write.WritePoint(point, _bucket, _organisation);
            }
            catch (Exception ex) {
                SentrySdk.CaptureException(ex);
            }
        }

        public async Task<Metric> Get(string stage)
        {
            var query = new Query(null,
                    "from(bucket: \"development\")" +
                         $"|> range(start: {DateTime.UtcNow.AddSeconds(-5).ToFileTimeUtc()}, stop: {DateTime.UtcNow.ToFileTimeUtc()})" +
                         "|> filter(fn: (r) => r[\"_field\"] == \"Duration\")" +
                         $"|> filter(fn: (r) => r[\"_measurement\"] == \"{stage}\")" +
                         $"|> aggregateWindow(every: {TimeSpan.FromSeconds(10)}, fn: mean, createEmpty: false)" +
                         "|> yield(name: \"mean\")\")" 
            );
            var x =  await _query.QueryAsync(query, "my-org");
            return null;
        }

        public void Dispose()
        {
            _write?.Flush();
            _write?.Dispose();
            _client?.Dispose();
        }
    }
}    