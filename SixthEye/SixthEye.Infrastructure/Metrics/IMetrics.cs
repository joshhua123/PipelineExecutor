﻿using System;
using System.Threading.Tasks;

namespace SixthEye.Infrastructure.Metrics
{
    public class Metric
    {
        
    }
    public interface IMetrics : IDisposable
    { 
        void Report(string source, string measurement, object fields);
        Task<Metric> Get(string stage);
    }
}