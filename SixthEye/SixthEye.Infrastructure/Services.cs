﻿using System;
using System.Data; 
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Configuration;
using RabbitMQ.Client; 
using SixthEye.Infrastructure.Application; 
using SixthEye.Infrastructure.Metrics;
using SixthEye.Infrastructure.ObjectStorage;
using SixthEye.Infrastructure.Pipeline;
using SixthEye.Infrastructure.Pipeline.Backlog;
using SixthEye.Infrastructure.RecordStore.Impl;
using ConnectionFactory = RabbitMQ.Client.ConnectionFactory;

namespace SixthEye.Infrastructure
{
    public static class Services
    { 
        public static readonly string ServerHost = "10.0.0.101";  
        public static readonly IServiceProvider Provider;
 
        static Services() {
            var services = new ServiceCollection();

            services.AddLogging(builder =>
            { 
                builder.AddConfiguration();
            });

            services.AddSingleton(new LoggerFactory().CreateLogger("SixthEye")); 
            
            services.AddSingleton(new StageReportingRegister()); 
 
            services.AddSingleton<IObjectStorage>(p => new LocalStorage("/Users/joshua/Pipelines"));

            services.AddSingleton<IBacklog>(_ => new SqliteBacklog());
            services.AddSingleton<IAppLauncher>(_ => new ObjectStoreLauncher());
            services.AddSingleton<ApplicationLibrary>();
            
            services.AddSingleton<IRecordStore>(_ => new SqliteRecord());
              
            services.AddSingleton<IConnectionFactory>(new ConnectionFactory { 
                HostName = ServerHost,
                AutomaticRecoveryEnabled = true,
                UseBackgroundThreadsForIO = true, 
                // NOTE This causes out of order messaging
                ConsumerDispatchConcurrency = 4,
                Password = "guest",
                UserName = "guest"
            });
             
            services.AddSingleton<IMetrics>(new NullMetrics()); //_ => new InfluxMetrics("http://" + ServerHost + ":8086", "development", "CyNH96QbCjpfGpB58ibReEBl9QIVyy3osYgT7XpcN2PSD1qxsjWTG-wsBekUDwvKsNGoZi-cTG4PHB5iW4iH-Q=="));
            
            Provider = services.BuildServiceProvider();  
        } 
 
        public static ILogger Log => Provider.GetService<ILogger>();
        public static IConnectionFactory RabbitMqFactory => Provider.GetService<IConnectionFactory>(); 
        public static IBacklog Backlog => Provider.GetService<IBacklog>(); 
        public static IAppLauncher Launcher => Provider.GetService<IAppLauncher>();
        public static ApplicationLibrary Library => Provider.GetService<ApplicationLibrary>();
        public static IObjectStorage ObjectStorage => Provider.GetService<IObjectStorage>();
        public static IRecordStore RecordStorage => Provider.GetService<IRecordStore>();
        public static IDbConnection Database => Provider.GetService<IDbConnection>(); 
        public static StageReportingRegister ReportingRegister => Provider.GetService<StageReportingRegister>(); 
        public static IRecordStore Couch => Provider.GetService<IRecordStore>();  
        public static IMetrics Metrics => Provider.GetService<IMetrics>(); 
    }
}