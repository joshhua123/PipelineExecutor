using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks; 
using Newtonsoft.Json;
using PipelineService.Feeders;
using PipelineService.Plugins;
using RabbitMQ.Client;
using SixthEye.Infrastructure.Pipeline;
using SixthEye.Infrastructure.Pipeline.Source;
using SixthEye.Infrastructure.Pipeline.Target;
using YamlDotNet.Serialization;
using Node = System.Collections.Generic.IDictionary<object, object>;

namespace SixthEye.Infrastructure.Application;

    public class ObjectStoreLauncher : IAppLauncher
    {
        // Converts a <object, object> into <string, string> flattening any lists into JSON strings
        private IDictionary<string, string> Stringify(Node node) {
            return node
                ?.Select((pair) => new KeyValuePair<string,string>(pair.Key as string, pair.Value as string ?? JsonConvert.SerializeObject(pair.Value)))
                ?.ToDictionary(x => x.Key, y => y.Value) ?? new Dictionary<string, string>();
        }
        
        private async Task<IPlugin> GetPlugin(string module, string function, IDictionary<string, string> config)
        {
            // Load module from ObjectStorage, firs

            await using var content = await Services.ObjectStorage.Get(module + ".dll", "modules")
                                      ?? throw new Exception("Unable to get module assembly");
            await using var memory = new MemoryStream();
            await content.CopyToAsync(memory);

            var assembly = Assembly.Load(memory.GetBuffer());

            var plugins = assembly.ExportedTypes
                .Where(x => x.IsAssignableTo(typeof(IPlugin)))
                .ToList();
            
            // Dependency Injection
            var plug = plugins.FirstOrDefault();
            var constructor = plug.GetConstructors();
            var parameters = constructor[0].GetParameters();

            var injection = parameters.Select(x => Services.Provider.GetService(x.ParameterType)).ToArray();
            
            var plugin = constructor[0].Invoke(injection) as IPlugin
                         ?? throw new Exception("Invalid plugin initialization");
            
            config.TryAdd("module", module);
            plugin.Init(function, config);
              
            return plugin;
        }
        
        public async Task<IEnumerable<Actor>> Launch(string appName)
        {
            var file = await Services.ObjectStorage.Get("pipeline.yml", Path.Join("applications", appName)); 
            var tree = new Deserializer().Deserialize(new StreamReader(file)) as Node;

            // The code below processes the .yml file and builds a StagePipeline
            object pipelines = null;
            tree?.TryGetValue("pipelines", out pipelines);
            return (pipelines as IDictionary<object, object>)?.Select(pipelineConfig =>
            {
                var actorConfig = (pipelineConfig.Value as Node) ?? throw new InvalidCastException("Yaml actor is ill-defined");
                actorConfig.TryGetValue("alias", out var alias);
                actorConfig.TryGetValue("stages", out var stages);
                
                var actor = new Actor() {
                    alias = alias as string,
                    Pipeline = new StatePipeline()
                };
                
                Services.ReportingRegister.Register(actor.Pipeline.Counters());
                 
                foreach (var stageConfig in stages as Node ?? 
                                            throw new Exception($"Invalid Stages in {alias}"))
                {
                    var stageNode = stageConfig.Value as Node;
                    var key = stageConfig.Key as string;

                    switch (key) {
                        case "csvfeeder":
                        {
                            if (stageNode.TryGetValue("filePath", out var filePath) &&
                                stageNode.TryGetValue("frequency", out var cFrequency))
                            {
                                var csvFeeder = new CsvSource(filePath as string,
                                    TimeSpan.Parse(cFrequency as string ?? ""));
                                actor.SubscribedTopics.Add(csvFeeder);
                                actor.Pipeline.Subscribe(csvFeeder, stageNode.ToDictionary(
                                    x => x.Key as string ?? "",
                                    y => y.Value as string ?? ""));
                            }
                        }
                            break;
                        case "feeder":
                        {         
                            if (stageNode.TryGetValue("partition", out var partition) &&
                                stageNode.TryGetValue("frequency", out var cFrequency))
                            {
                                var warehouseFeeder = new WarehouseFeeder(partition as string,
                                    TimeSpan.Parse(cFrequency as string ?? ""));
                                actor.SubscribedTopics.Add(warehouseFeeder);
                                actor.Pipeline.Subscribe(warehouseFeeder, stageNode.ToDictionary(
                                    x => x.Key as string ?? "",
                                    y => y.Value as string ?? ""));
                            }
                            
                        }     
                            break;
                        case "subscribe": {
                            if (stageNode.TryGetValue("exchange", out var exchange) &&
                                stageNode.TryGetValue("queue", out var queue))
                            {
                                //var source = new SqliteSource(exchange as string, queue as string);
                                var source = new InMemorySource(exchange as string, queue as string);
                                actor.SubscribedTopics.Add(source); 
                                actor.Pipeline.Subscribe(source, stageNode.ToDictionary(
                                    x => x.Key as string ?? "",
                                    y => y.Value as string ?? ""));
                                
                                Services.ReportingRegister.Register(source.Counters());
                            }
                            else
                                throw new Exception($"Missing parameters for {key}");
                        }
                            break;
                        case "then": {
                            if (stageNode.TryGetValue("module", out var module) &&
                                stageNode.TryGetValue("function", out var function) &&
                                stageNode.TryGetValue("config", out var config))
                            {
                                var plugin = GetPlugin(module as string, function as string, Stringify(config as Node))
                                    .GetAwaiter().GetResult();
                                actor.Plugins.Add(plugin);
                                actor.Pipeline.Then(plugin, stageNode.ToDictionary(
                                    x => x.Key as string ?? "",
                                    y => y.Value as string ?? "")); 
                                Services.ReportingRegister.Register(plugin.Counters());
                            }
                            else
                                throw new Exception($"Missing parameters for {key}");
                        }
                            break;
                        case "publish": {
                            if (stageNode.TryGetValue("exchange", out var exchange) &&
                                stageNode.TryGetValue("queue", out var queue))
                            {
                                //var target = new SqliteTarget(exchange as string, queue as string);
                                var target = new InMemoryTarget(exchange as string, queue as string);
                                actor.PublishedTopics.Add(target);
                                actor.Pipeline.Publish(target, stageNode.ToDictionary(
                                        x => x.Key as string ?? "",
                                        y => y.Value as string ?? ""));
                                
                                Services.ReportingRegister.Register(target.Counters());
                            }
                            else
                                throw new Exception($"Missing parameters for {key}");
                        }
                            break;
                        case "record": {
                            if (stageNode.TryGetValue("partition", out var partition))
                            {
                                //var target = new SqliteTarget(exchange as string, queue as string);
                                var target = new RecordTarget(partition as string);
                                actor.PublishedTopics.Add(target);
                                actor.Pipeline.Publish(target, stageNode.ToDictionary(
                                    x => x.Key as string ?? "",
                                    y => y.Value as string ?? ""));
                                Services.ReportingRegister.Register(target.Counters());
                            }
                            else
                                throw new Exception($"Missing parameters for {key}");
                        }
                            break;
                        case "terminate":
                        {
                            actor.Pipeline.Publish(new TerminatorTarget(), stageNode.ToDictionary(
                                x => x.Key as string ?? "",
                                y => y.Value as string ?? ""));
                        }
                            break;
                    }  
                }
                return actor; 
            }).ToArray(); 
        }
    }