using System;
using System.Collections.Generic;

namespace SixthEye.Infrastructure.Application;

public class AppInstance : IDisposable
{
    public string ApplicationName { get; set; }
    public List<Actor> Actors { get; set; } = new();
 
    public void Dispose() {
        foreach (var actor in Actors) {
            actor.Pipeline.CancellationTokenSource.Cancel();
        }
    }
}