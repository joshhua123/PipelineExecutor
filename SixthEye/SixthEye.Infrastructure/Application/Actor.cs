﻿using System;
using System.Collections.Generic;
using System.Linq;
using PipelineService.Plugins; 
using SixthEye.Infrastructure.Pipeline;
using SixthEye.Infrastructure.Pipeline.Source;
using SixthEye.Infrastructure.Pipeline.Target;

namespace SixthEye.Infrastructure.Application;

public class Actor : IDisposable
{
    public Guid id { get; } = Guid.NewGuid(); 
    public string alias { get; init; }
    public StatePipeline Pipeline { get; init; }  
    public List<IPipelineSource<State>> SubscribedTopics { get; set; } = new();
    public List<IPipelineTarget<State>> PublishedTopics { get; set; } = new(); 
    public List<IPlugin> Plugins { get; set; } = new();

    public void Dispose()
    {
        Pipeline.CancellationTokenSource.Cancel();
        Pipeline.CancellationTokenSource.Dispose();
        foreach (var exe in Plugins) {
            exe.Dispose();
        }
    }

    public object SelfReport()
    {
        return new
        {
            id,
            alias,
            SubscribedTopics = SubscribedTopics.Select(x => x.SelfReport()), 
            PublishedTopics = PublishedTopics.Select(x => x.SelfReport()), 
            Plugins = Plugins.Select(x => x.SelfReport()),
            Pipeline = Pipeline.SelfReport()
        };
    }
} 