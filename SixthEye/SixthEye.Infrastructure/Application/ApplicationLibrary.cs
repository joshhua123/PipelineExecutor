using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SixthEye.Infrastructure.ObjectStorage;

namespace SixthEye.Infrastructure.Application
{
    public class ApplicationLibrary {
        private readonly IObjectStorage _storage;
        private readonly IAppLauncher _launcher;

        public readonly ConcurrentDictionary<string, AppInstance> Running = new();

        public ApplicationLibrary(IObjectStorage storage, IAppLauncher launcher)
        {
            _storage = storage;
            _launcher = launcher;
        }

        /// <summary>
        /// List application files saved within the storage backend 
        /// </summary>
        public async Task<IEnumerable<string>> List()
        {
            return await _storage.List("applications");
        }

        /// <summary>
        /// Download/Extract/Execute
        /// This will launch an application on this node
        /// </summary>
        public async Task<AppInstance> Start(string appName)
        {
            if (Running.TryGetValue(appName, out var existing))
                return existing;
                
            var instance = new AppInstance() {
                ApplicationName = appName,
                Actors = (await _launcher.Launch(appName)).ToList()
            };

            if (Running.TryAdd(appName, instance))
                return instance;
            instance.Dispose();
            throw new Exception("Application already running");
        }
        
        public AppInstance Stop(string appName) {
            if (Running.TryRemove(appName, out var instance)) { 
                instance.Dispose();
                return instance;
            }

            throw new Exception($"Application wasn't running {appName}");
        }
    }
}