using System.Collections.Generic;
using System.Threading.Tasks;
using SixthEye.Infrastructure.Pipeline;

namespace SixthEye.Infrastructure.Application
{
    public interface IAppLauncher {
        /// <summary>
        /// Launches all pipelines of an Application
        /// </summary>
        /// <param name="appName"></param>
        /// <returns></returns>
        public Task<IEnumerable<Actor>> Launch(string appName);
    }
}