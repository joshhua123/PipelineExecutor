using System; 
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Threading.Tasks; 
using SixthEye.Infrastructure.Executor; 

namespace SixthEye.Infrastructure.Application
{
    /// <summary>
    /// A IAppLauncher that'll be able to launch packaged applications from a compressed format
    /// </summary>
    public class ZipLauncher : IAppLauncher
    { 
        private IExecutor _executor; 

        private IDictionary<string, MemoryStream> _applications;

        public ZipLauncher() { 
            //_executor = new JeringJSExecutor();
            _applications = null; // _objectStorage.List().Result.ToDictionary(x => x, x => null as MemoryStream); 
        }

        public async Task<IEnumerable<Actor>> Launch(string appName)
        {
            throw new NotImplementedException();
            
            if(!_applications.TryGetValue(appName, out var application))
                throw new Exception("Application not found");

            //await (await _objectStorage.Get(appName)).CopyToAsync(application);

            var archive = new ZipArchive(application);
            
            // Load WASM modules
            // TODO This will probably be loaded into a registery which deduplicates loaded modules
            // 
            var moduleFiles = archive.Entries
                .Where(x => x.FullName.StartsWith("/app")).ToList();

            // Launch
            var index = archive.Entries.Single(x => x.Name.Equals("index.js"));
            await using var indexStream = index.Open();
            await using var memoryStream = new MemoryStream();
            await indexStream.CopyToAsync(memoryStream);
            //_executor.Build(memoryStream.ToArray(), "");
            //_executor.Run(null);
             
        }

        public ICollection<string> ListInstalled() {
            return _applications.Keys;
        }
         
    }
}