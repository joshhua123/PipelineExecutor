using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading;
using System.Threading.Tasks.Dataflow;
using SixthEye.Infrastructure.Application;

namespace SixthEye.Infrastructure.Pipeline;

/// <summary>
/// The Backlog is a mechanism to handle state that was unable to fit into the working pipeline 
/// </summary>
public interface IBacklog : IDisposable
{
    /// <summary>
    /// Add state to backlog
    /// </summary>
    /// <param name="queue"></param>
    /// <param name="state"></param>
    public void Send(string queue, State state); 

    /// <summary>
    /// Starts a task to periodically fill the @target with state within the @queue
    /// </summary>
    /// <param name="queue"></param>
    /// <param name="target"></param>
    /// <param name="reporting"></param>
    /// <param name="cancellationToken"></param>
    public void StartInfill(string queue, ITargetBlock<State> target, StageReporting reporting, CancellationToken cancellationToken);
}