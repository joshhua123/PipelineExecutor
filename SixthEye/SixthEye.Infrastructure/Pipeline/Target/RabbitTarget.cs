﻿using System;
using System.Text;
using MessagePack;
using Newtonsoft.Json;
using RabbitMQ.Client;
using Sentry;
using SixthEye.Infrastructure.Application; 

namespace SixthEye.Infrastructure.Pipeline.Target;

/// <summary>
/// A pipeline target that publishes to a RabbitMQ queue
/// </summary>
public class RabbitTarget : IPipelineTarget<State>
{  
    private IModel Model { get ; }
    private readonly IConnection _connection;
     
    private readonly StageReporting _reporting = new(); 
    
    private readonly string _routingKey = Guid.NewGuid().ToString();
         
    private readonly string _exchange;
    private readonly string _queue;
        
    public RabbitTarget(string exchange, string queue)
    {
        _connection = Services.RabbitMqFactory.CreateConnection(); 
        _exchange = exchange;
        _queue = queue;
         
        Model = _connection.CreateModel();  
        
        Model.ExchangeDeclare(exchange, "direct", true);
        // Create queue
        Model.QueueDeclare(queue, true, false, false, null);
        // Bind our publish commands to this specific queue
        Model.QueueBind(queue, exchange, _routingKey); 
    }

    public void Send(State value)
    {
        if (value is null) {
            _reporting.Dropped++;
            return;
        }

        try
        {
            // todo Do we need a lock here?
            lock (Model) {
                Model.BasicPublish(
                    _exchange,
                    _routingKey,
                    null,
                    MessagePackSerializer.Serialize(value)
                );
            }

            _reporting.AppendPeek(value);
            _reporting.Processed++;
        }
        catch (Exception ex)
        {
            _reporting.Fatal++;
            SentrySdk.CaptureException(ex);
        }
    }
        
    public void Dispose()
    {
        _connection?.Dispose();
        Model?.Dispose();
    }
  
    public object SelfReport()
    {
        return new { exchange = _exchange, queue = _queue };
    }

    public StageReporting Counters()
    {
        return _reporting;
    }
}