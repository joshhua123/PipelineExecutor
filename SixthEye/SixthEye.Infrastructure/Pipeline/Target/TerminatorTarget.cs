using SixthEye.Infrastructure.Application;

namespace SixthEye.Infrastructure.Pipeline.Target;

public class TerminatorTarget : IPipelineTarget<State>
{
    private StageReporting _reporting = new(); 
    public void Send(State value)
    {
        _reporting.Processed++;
    }

    public object SelfReport() {
        return new { processed = _reporting.Processed};
    }

    public StageReporting Counters()
    {
        return _reporting;
    }

    public void Dispose()
    {
    }
}