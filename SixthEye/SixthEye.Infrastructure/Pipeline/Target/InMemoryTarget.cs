using System.Collections.Concurrent;
using SixthEye.Infrastructure.Application;

namespace SixthEye.Infrastructure.Pipeline.Target;

public class InMemoryTarget : IPipelineTarget<State>
{
    private readonly StageReporting _reporting = new(); 
    private readonly ConcurrentQueue<State> _queue;
    private readonly string _queueName;

    public InMemoryTarget(string exchange, string queue) { 
        _queue = InMemoryShared.GetOrCreateExchange(exchange).GetOrCreateQueue(queue);
        _queueName = queue;
    }
    
    public void Send(State value)
    {
        if(_queue.Count <= 512) {
            _queue.Enqueue(value);
            _reporting.Processed++;
        } else {
            Services.Backlog.Send(_queueName, value);
            _reporting.Stall++;
        }
    }

    public object SelfReport() {
        return new {  _queueName, _queue.Count};
    }

    public StageReporting Counters() => _reporting;

    public void Dispose() { 
    }
}