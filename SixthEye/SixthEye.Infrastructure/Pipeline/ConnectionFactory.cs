using System.Data;
using Microsoft.Data.Sqlite;

namespace SixthEye.Infrastructure.Pipeline;

public static class ConnectionFactory
{
    //private static Dictionary<string, SqliteConnection> _connections = new();
    private static SqliteConnection _memory; 

    public static SqliteConnection GetConnection(string exchange)
    {
        if (":memory:".Equals(exchange))
        {
            var con = new SqliteConnection($"Data Source=:memory:?cache=shared");
            con.Open();
            return con;
            if(_memory.State != ConnectionState.Open)
                _memory.Open();
            return _memory;
        }
        
        var conn = new SqliteConnection($"Data Source={exchange}");
        conn.Open();
        return conn;
        
       // lock (_connections)
       // { 
       //     if (!_connections.TryGetValue(exchange, out var conn))
       //     {
       //         if (!_connections.ContainsKey(exchange))
       //         {
       //             //if (!exchange.Equals(":memory:"))
       //             //    exchange = Path.Join("./data/", exchange);
       //             
       //             _connections.Add(exchange, conn);
       //         }
       //     } 
       //     return conn;
       // }
    } 
}