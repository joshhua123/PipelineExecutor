using System;
using System.Collections.Generic;
using System.Diagnostics; 
using System.Threading.Tasks;
using System.Threading.Tasks.Dataflow;
using Microsoft.Extensions.Logging; 
using SixthEye.Infrastructure.Pipeline.Source;
using SixthEye.Infrastructure.Pipeline.Target;

namespace SixthEye.Infrastructure.Pipeline
{
    // A simple pipeline doesn't contain any branching
    public class SimplePipeline<sT> : Pipeline<sT>
    {
        public SimplePipeline() : this(new BufferBlock<sT>())
        {
        }

        public SimplePipeline(IDataflowBlock lastNode) : base(lastNode as BufferBlock<sT>)
        {
            /*  TODO
                The define function's purpose is to remove the source
                This prevents us from modifying the source block out of order
                right now I'm copying it across for development speed
                but the goal here may be to accept the IPipelineSource as a constructor header
                (or define argument) so we can preserve that rule. 
            */  
            _last_node = lastNode;
            _bufferBlocks.Add(_first_buffer); 
        }

        public SimplePipeline(BufferBlock<sT> block) : base(block as BufferBlock<sT>)
        { 
            _last_node = block;
        }





        public SimplePipeline<TOut> Then<TOut>(Func<sT, TOut> func, string identifier)
        {
            Install(TransformWrap(func, identifier));
            return new SimplePipeline<TOut>(_last_node);
        }

        public SimplePipeline<TOut[]> Then<TOut>(Func<sT, TOut[]> func, string identifier)
        {
            Install(TransformWrap(func, identifier));
            return new SimplePipeline<TOut[]>(_last_node);
        }

        public SimplePipeline<sT> Branch(Func<sT, bool> func, Action<SimplePipeline<sT>> branch, string identifier)
        {
            var branchBuffer = new BufferBlock<sT>();
            var block = new TransformManyBlock<sT, sT>((value) =>
            {
                if (func(value))
                {
                    branchBuffer.Post(value);
                    return Array.Empty<sT>();
                }

                return new[] {value};
            });

            branch(new SimplePipeline<sT>(branchBuffer));

            Install(block);
            return this;
        }

        public SimplePipeline<sT> Filter(Predicate<sT> func, string identifier)
        {
            var block = new TransformManyBlock<sT, sT>((value) =>
            {
                if (func(value))
                {
                    return Array.Empty<sT>();
                }

                return new[] {value};
            });

            Install(block);
            return this;
        }

        public SimplePipeline<sT> Then(Action<sT> action, string identifier)
        {
            var block = new ActionBlock<sT>(Wrap(action, identifier));
            LastNode.LinkTo(block);
            return null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public SimplePipeline<sT> Subscribe(IPipelineSource<sT> source, string identifier)
        {
            source.Bind(_first_buffer);  
            return this;
        }

        public SimplePipeline<T> Publish<T>(IPipelineTarget target, Func<sT, T> func, string identifier)
        {
            return Then((sT value) =>
            {
                var result = func.Invoke(value);
                target.Send(result);
                return result;
            }, identifier);
        }
        
        public SimplePipeline<T[]> PublishMany<T>(IPipelineTarget target, Func<sT, Task<T[]>> func, string identifier)
        {
            // TODO This should break the return type to T, we'll need a ThenMany
            return Then((sT value) =>
            {
                var awaiter = func.Invoke(value);
                awaiter.Wait(); // TODO What to do about Async, there must be a better way, this won't scale
                var result = awaiter.Result;
                foreach (var item in result)
                    target.Send(item); 
                return result;
            }, identifier);
        }
        
        public SimplePipeline<T[]> PublishMany<T>(IPipelineTarget target, Func<sT, T[]> func, string identifier)
        {
            // TODO This should break the return type to T, we'll need a ThenMany
            return Then((sT value) =>
            {
                var result = func.Invoke(value);  
                foreach (var item in result) 
                    target.Send(item); 
                return result;
            }, identifier);
        }

        public SimplePipeline<sT> Define()
        {
            var factory = new SimplePipeline<sT>(_last_node);
            _last_node = null;
            return factory;
        }

        public bool Feed(sT value)
        {
            return _first_buffer.Post(value);
        } 
    }
}