﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Threading.Tasks.Dataflow;
using SixthEye.Infrastructure.Pipeline.Source;
using SixthEye.Infrastructure.Pipeline.Target;

namespace SixthEye.Infrastructure.Pipeline
{
    public class JsonPipeline : Pipeline<string>
    {

        public object Debug
        {
            get {
                return _bufferBlocks;
            }
        }

        public JsonPipeline() : base(new BufferBlock<string>()) {
            _last_node = _first_buffer;
        } 

        public void Then<TOut>(Func<string, TOut> func, string identifier)
        {
            Install(TransformWrap(func, identifier)); 
        }
        
       // public void Then<TOut>(Func<string, Task<TOut>> func, string identifier) where TOut : Task
       // {
       //     var transformer = TransformWrapAsync(func, identifier);
       //     InstallAsync(transformer); 
       // }

        public void ThenMany<TOut>(Func<string, TOut[]> func, string identifier)
        {
            Install(TransformWrap(func, identifier));
        }

        //public void Branch(Func<string, bool> func, Action<JsonPipeline> branch)
        //{
        //    var branchBuffer = new BufferBlock<string>();
        //    var block = new TransformManyBlock<string, string>((value) =>
        //    {
        //        if (func(value))
        //        {
        //            branchBuffer.Post(value);
        //            return Array.Empty<string>();
        //        }
//
        //        return new[] {value};
        //    });
//
        //    branch(new JsonPipeline(branchBuffer));
//
        //    Install(block); 
        //}

        public void Filter(Predicate<string> func, string identifier)
        {
            var block = new TransformManyBlock<string, string>((value) =>
            {
                if (func(value))
                {
                    return Array.Empty<string>();
                }

                return new[] {value};
            });

            Install(block); 
        }

        public void Then(Action<string> action, string identifier)
        {
            var block = new ActionBlock<string>(Wrap(action, identifier));
            LastNode.LinkTo(block); 
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public void Subscribe(IPipelineSource<string> source, string identifier)
        {
            source.Bind(_first_buffer);
        }

        public void Publish<T>(IPipelineTarget target, Func<string, T> func, string identifier)
        {
            Then((string value) =>
            {
                var result = func.Invoke(value);
                target.Send(result); 
            }, identifier);
        }
        
        public void PublishMany<T>(IPipelineTarget target, Func<string, Task<T[]>> func, string identifier)
        {
            // TODO This should break the return type to T, we'll need a ThenMany
            ThenMany((string value) =>
            {
                var awaiter = func.Invoke(value);
                awaiter.Wait(); // What to do about Async, there must be a better way, this won't scale
                var result = awaiter.Result;
                foreach (var item in result)
                    target.Send(item); 
                return result;
            }, identifier);
        }
        
        public void PublishMany<T>(IPipelineTarget target, Func<string, T[]> func, string identifier)
        {
            // TODO This should break the return type to T, we'll need a ThenMany
            ThenMany((string value) =>
            {
                var result = func.Invoke(value);  
                foreach (var item in result) 
                    target.Send(item); 
                return result;
            }, identifier);
        } 

        public bool Feed(string value, string identifier)
        {
            return _first_buffer.Post(value);
        }

        public override string ToString()
        {
            return "encoded";
        }
    }
}