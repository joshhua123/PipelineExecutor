using System.Collections.Concurrent;
using SixthEye.Infrastructure.Application;

namespace SixthEye.Infrastructure.Pipeline;
public class InMemoryExchange
{
    public string Exchange;
    readonly ConcurrentDictionary<string, ConcurrentQueue<State>> _queues = new();
    public ConcurrentQueue<State> GetOrCreateQueue(string queue) {
        return _queues.GetOrAdd(queue, (_) => new ConcurrentQueue<State>());
    }
}

/// <summary>
/// This class is used to share data-structures between @InMemorySource and @InMemoryTarget 
/// </summary>
internal static class InMemoryShared { 
    static readonly ConcurrentDictionary<string, InMemoryExchange> _exchanges = new(); 
    public static InMemoryExchange GetOrCreateExchange(string exchange) {
        return _exchanges.GetOrAdd(exchange, (_) => new InMemoryExchange());
    }
}