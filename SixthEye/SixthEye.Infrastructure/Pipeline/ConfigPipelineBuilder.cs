using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using SixthEye.Infrastructure.Application;
using Sentry;
using SixthEye.Infrastructure.Executor;
using SixthEye.Infrastructure.Pipeline.Source;
using SixthEye.Infrastructure.Pipeline.Target;

namespace SixthEye.Infrastructure.Pipeline;

public static class ConfigPipelineBuilder { 
    public static StatePipeline Subscribe(this StatePipeline pipeline, IPipelineSource<State> source, IDictionary<string, string> args)
    { 
        pipeline.Subscribe(source, new StageConfig() {
            Identifier = Guid.NewGuid().ToString(),
            Parameters = args, //  parameters,
            Module = nameof(source), Function = nameof(Subscribe),
            ReportingIdentifier = source.Counters().Id.ToString()
        });
        
        return pipeline;
    }

    private static int? TryGetInt(IDictionary<string, string> args, string key) { 
            if (args.TryGetValue(key, out var value) &&
                Int32.TryParse(value, out var parsedValue)) {
                return parsedValue;
            }
            return null;
    }
    
    public static StatePipeline Then(this StatePipeline pipeline, IExecutor executor, IDictionary<string, string> args)
    { 
        var maxConcurrency = TryGetInt(args, "maxConcurrency");
        var maxCapacity  = TryGetInt(args, "maxCapacity");
         
        pipeline.ThenMany(state => { 
            var task = executor.Run(state);  
            
            task.Wait(Convert.ToInt32(TimeSpan.FromSeconds(60).TotalMilliseconds), pipeline.CancellationTokenSource.Token); 
            if (task.IsCompletedSuccessfully)
                return task.Result;

            if (task.IsFaulted)
                if(task.Exception is not null)
                    SentrySdk.CaptureException(task.Exception);  
  
            // TODO we're going to timeout packets here, I want the subscription service to tag their source queue
            // A dropped packet will be returned to the start. 
            executor.Counters().Dropped++;
            return Array.Empty<State>(); 
        }, new StageConfig()
        {
            Identifier = Guid.NewGuid().ToString(),
            Parameters = args,
            MaxConcurrency = maxConcurrency,
            MaxCapacity = maxCapacity,
            Module = nameof(executor), Function = "", // Source these from the IExecutor
            ReportingIdentifier = executor.Counters().Id.ToString()
        });
         
        return pipeline;
    }

    public static StatePipeline Publish(this StatePipeline pipeline, IPipelineTarget<State> target, IDictionary<string, string> args)
    {        
        pipeline.Publish(target, x => x, new StageConfig() {
            Identifier = Guid.NewGuid().ToString(),
            Parameters = args, // parameters,
            Module = nameof(target), Function = nameof(Publish),
            ReportingIdentifier = target.Counters().Id.ToString()
        });
        
        return pipeline;
    } 
}