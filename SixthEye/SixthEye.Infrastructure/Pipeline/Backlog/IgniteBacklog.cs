using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using System.Threading.Tasks.Dataflow;
using Apache.Ignite.Core;
using Apache.Ignite.Core.Messaging;
using Sentry;
using SixthEye.Infrastructure.Application;

namespace SixthEye.Infrastructure.Pipeline.Backlog;

public class IgniteBacklog : IBacklog
{
    private readonly IIgnite _ignite;
    private readonly IMessaging _messaging;
    private readonly ConcurrentBag<Task> _runningTasks = new();
    private readonly Dictionary<string, bool> _listeners = new();
    IgniteBacklog()
    {
        _ignite = Ignition.GetIgnite("backlog");
        _messaging = _ignite.GetMessaging();
    }
    public void Dispose()
    {
        _ignite?.Dispose();
        
    }

    public void Send(string queue, State state)
    {
        _messaging.Send(state, queue);
    }

    public void StartInfill(string queue, ITargetBlock<State> target, StageReporting reporting, CancellationToken cancellationToken)
    {
        _runningTasks.Add(Task.Factory.StartNew(() => {
            long msgId = 1;
            var tBlock = target as ITargetBlock<State> ?? throw new InvalidCastException($"{nameof(BufferBlock<State>)} can't be cast to {nameof(ITargetBlock<State>)}");
            var cycle = Stopwatch.StartNew();
        
            InfillLoop:

            cycle.Restart();

            try
            { 
                // If we successfully send a state object, try adding another from the backlog
                // we need to ack before sending to prevent duplication
                var peekList = Peek(queue);
                if (peekList.Count == 0)
                    Thread.Sleep(TimeSpan.FromSeconds(5));
                foreach (var infill in peekList) {
                    if (infill is not null && Ack(queue, infill)) {
                        switch (tBlock.OfferMessage(new DataflowMessageHeader(msgId++),
                                    infill, null, false)) {
                            case DataflowMessageStatus.Accepted:
                                reporting.Processed++;
                                break;
                            case DataflowMessageStatus.DecliningPermanently:
                                return;
                            default:
                                Send(queue, infill); 
                                Thread.Sleep(TimeSpan.FromMilliseconds(500));
                                break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                SentrySdk.CaptureException(ex);
            }

            cycle.Stop();

            Services.Metrics.Report(queue, "infill_ack", new { Duration = cycle.ElapsedMilliseconds });

            goto InfillLoop;
        }, TaskCreationOptions.LongRunning));  
    }

    private ConcurrentBag<State> _received = new ConcurrentBag<State>();

    public ICollection<State> Peek(string queue)
    {
        return Array.Empty<State>();
        // if(_listeners.ContainsKey(queue)) {
        //     lock (_listeners) {
        //         if(_listeners.ContainsKey(queue)){
        //             _messaging.LocalListen<State>(new LocalEventListener<State>((state) =>
        //             {
        //                 _received.Add(state);
        //             }, queue);
        //             _listeners.Add(queue, true);
        //         }
        //     }
        // }
    }

    public bool Ack(string queue, State state)
    {
        throw new System.NotImplementedException();
    }
}