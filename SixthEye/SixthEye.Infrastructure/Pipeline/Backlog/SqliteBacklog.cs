using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Threading.Tasks.Dataflow;
using Microsoft.Data.Sqlite;
using Newtonsoft.Json;
using Sentry;
using SixthEye.Infrastructure.Application;

namespace SixthEye.Infrastructure.Pipeline.Backlog;

public class SqliteBacklog : IBacklog
{
    private readonly SqliteConnection _backlog;
    
    // Used to hold state in collections of queue
    private readonly ConcurrentDictionary<string, ConcurrentBag<State>> _buffer = new ();
    private readonly HashSet<string> _knownTables = new();
    private readonly ConcurrentBag<Task> _runningTasks = new();
    private const int BatchSize = 512;
  
    struct PeekPack {
        public SqliteCommand command; 
    }
     
    struct CommitPack {
        public SqliteCommand command;
        public SqliteParameter id;
        public SqliteParameter variables;
    }
    
    struct AckPack {
        public SqliteCommand command; 
        public SqliteParameter id;
    }
     
    private readonly ThreadLocal<PeekPack> _peekCommandCache; 
    private readonly ThreadLocal<CommitPack> _commitCommandCache;
    private readonly ThreadLocal<AckPack> _ackCommandCache;
    
    public SqliteBacklog() {
        _backlog = ConnectionFactory.GetConnection("backlog_v2.db");
        using var backCmd = _backlog.CreateCommand(); 
        backCmd.CommandText = "PRAGMA journal_mode=WAL";
        backCmd.ExecuteNonQuery();     
        
        
        _peekCommandCache = new ThreadLocal<PeekPack>(() => {
            var command = _backlog.CreateCommand(); 
            return new PeekPack() { command = command };
        });                 
        _commitCommandCache = new ThreadLocal<CommitPack>(() => {
            var command = _backlog.CreateCommand(); 
            return new CommitPack() {
                command = command,
                id = command.Parameters.Add("id", SqliteType.Text),
                variables = command.Parameters.Add("variables", SqliteType.Text)
            };
        });        
        _ackCommandCache = new ThreadLocal<AckPack>(() => {
            var command = _backlog.CreateCommand(); 
            return new AckPack() {
                command = command,
                id = command.Parameters.Add("id", SqliteType.Text)
            };
        });
    }

    private void EnsureTable(string queue)
    {
        if (_knownTables.Contains(queue))
            return;  
        lock(_knownTables) {
            if (_knownTables.Contains(queue))
                return; 
            using var backCmd = _backlog.CreateCommand();
            backCmd.CommandText =
                $"CREATE TABLE IF NOT EXISTS {queue} (id varchar PRIMARY KEY, variables varchar) WITHOUT ROWID";
            backCmd.ExecuteNonQuery();
            _knownTables.Add(queue);
        } 
    }

    private void Commit(ConcurrentBag<State> buffer, string queue)
    {
        lock (_backlog)
        {
            if(buffer.IsEmpty)
                return;
            
            EnsureTable(queue);
            
            try {
                
                using var transaction = _backlog.BeginTransaction(); 
                var pack = _commitCommandCache.Value;
                pack.command.Transaction = transaction;
                pack.command.CommandText = $"INSERT INTO {queue} (id, variables) VALUES ($id, $variables)";
 
                
                foreach (var backlogState in buffer)
                {
                    pack.id.Value = backlogState.Id;
                    pack.variables.Value = JsonConvert.SerializeObject(backlogState.variables); 
                    
                    pack.command.ExecuteNonQuery();
                }
                
                transaction.Commit();
                buffer.Clear();
            }
            catch(Exception ex) {
                SentrySdk.CaptureException(ex);
            }
        }
    }

    private ConcurrentBag<State> GetQueueBuffer(string queue)
    { 
        // If the buffer doesn't exist initialize
        if (!_buffer.TryGetValue(queue, out var queueBacklog)) {
            queueBacklog = new ConcurrentBag<State>();
            _buffer.TryAdd(queue, queueBacklog); 
        }

        return queueBacklog;
    }
    
    public void Send(string queue, State state)
    {
        var queueBacklog = GetQueueBuffer(queue);
        
        if (queueBacklog.Count > BatchSize) 
            Commit(queueBacklog, queue);  
            
        queueBacklog.Add(state);
    }

    public void StartInfill(string queue, ITargetBlock<State> target, StageReporting reporting, CancellationToken cancellationToken)
    { 
        _runningTasks.Add(Task.Factory.StartNew(() => {
            long msgId = 1;
            var tBlock = target as ITargetBlock<State> ?? throw new InvalidCastException($"{nameof(BufferBlock<State>)} can't be cast to {nameof(ITargetBlock<State>)}");
            var cycle = Stopwatch.StartNew();
        
            InfillLoop:

            cycle.Restart();

            try
            { 
                // If we successfully send a state object, try adding another from the backlog
                // we need to ack before sending to prevent duplication
                var peekList = Peek(queue);
                if (peekList.Count == 0)
                    Thread.Sleep(TimeSpan.FromSeconds(5));
                foreach (var infill in peekList) {
                    if (infill is not null && Ack(queue, infill)) {
                        switch (tBlock.OfferMessage(new DataflowMessageHeader(msgId++),
                                    infill, null, false)) {
                            case DataflowMessageStatus.Accepted:
                                reporting.Processed++;
                                break;
                            case DataflowMessageStatus.DecliningPermanently:
                                return;
                            default:
                                Send(queue, infill); 
                                Thread.Sleep(TimeSpan.FromMilliseconds(500));
                                break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                SentrySdk.CaptureException(ex);
            }

            cycle.Stop();

            Services.Metrics.Report(queue, "infill_ack", new { Duration = cycle.ElapsedMilliseconds });

            goto InfillLoop;
        }, TaskCreationOptions.LongRunning));  
    }

    public ICollection<State> Peek(string queue)
    {  
        // VTUNE has this has a high contested function responsible for the majority of thread waits
        EnsureTable(queue);

        var cache = _peekCommandCache.Value;
        cache.command.CommandText = $"SELECT id, variables FROM  {queue} LIMIT 512";
  
        using var reader = cache.command.ExecuteReader();

        ICollection<State> result = null;
        
        while(reader.Read()) {
            result ??= new List<State>(512);
            result.Add(new State() {
                Id = reader.GetString(0),
                variables =
                    JsonConvert.DeserializeObject<IDictionary<string, string>>(reader.GetString(1))
            });
        }

        return result ?? Array.Empty<State>();
    }

    public bool Ack(string queue, State state)
    {
        var pack = _ackCommandCache.Value;
        pack.command.CommandText = $"DELETE FROM {queue} WHERE id=$id";
        pack.id.Value = state.Id; 
        return pack.command.ExecuteNonQuery() == 1;
    }

    public void Dispose()
    { 
        lock(_backlog)
        {
            foreach (var pack in _peekCommandCache.Values)
                pack.command?.Dispose();
            foreach (var pack in _ackCommandCache.Values)
                pack.command?.Dispose();
            foreach (var pack in _commitCommandCache.Values)
                pack.command?.Dispose();
        }
        
        
        _backlog?.Dispose();
        _peekCommandCache?.Dispose();
        _ackCommandCache?.Dispose();
        _commitCommandCache?.Dispose();
    }
}