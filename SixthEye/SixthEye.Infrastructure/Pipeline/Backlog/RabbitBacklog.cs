using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks.Dataflow;
using MessagePack;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using Sentry;
using SixthEye.Infrastructure.Application;

namespace SixthEye.Infrastructure.Pipeline.Backlog;

public class RabbitBacklog : IBacklog
{
    private readonly string _exchange;  
    
    private IConnection Connection { get; }
    private IModel Model { get ; } 
    private readonly HashSet<string> _existingQueues = new ();
    private readonly List<EventingBasicConsumer> _infillConsumers = new();
    private long _msgId = 1;

    public void Dispose() { 
        foreach(var consumer in _infillConsumers) {
            foreach (var tag in consumer.ConsumerTags) {
                Model.BasicCancelNoWait(tag);
            }
        } 
        
        Model?.Dispose();
    }

    public RabbitBacklog(string exchange)
    { 
        _exchange = string.Intern(exchange); 
        Connection= Services.RabbitMqFactory.CreateConnection();
        Model = Connection.CreateModel();

        Connection.ConnectionShutdown += (sender, args) =>
        {
            SentrySdk.CaptureEvent(new SentryEvent()
            {
                Level = SentryLevel.Fatal,
                Message = new SentryMessage {
                    Message = $"Shutdown = text:{args.ReplyText} initiator:{args.Initiator} cause:{JsonConvert.SerializeObject(args.Cause)}"
                }
            });
        };
        
        Connection.ConnectionBlocked +=  (sender, args) =>
        {
            SentrySdk.CaptureEvent(new SentryEvent()
            {
                Level = SentryLevel.Fatal,
                Message = new SentryMessage {
                    Message = $"Blocked = reason:{args.Reason}"
                }
            });
        };
        
        Connection.ConnectionUnblocked +=  (sender, args) =>
        {
            SentrySdk.CaptureEvent(new SentryEvent()
            {
                Level = SentryLevel.Fatal,
                Message = new SentryMessage {
                    Message = $"Unblocked = reason:{args}"
                }
            });
        }; 
        
        Model.ExchangeDeclare(exchange, "direct", true);   
    }
  
    public void Send(string queue, State state)
    {
        if(!_existingQueues.Contains(queue)) {
            Model.QueueDeclare(queue, true, false, false, null);
            Model.QueueBind(queue, _exchange, queue); 
            _existingQueues.Add(queue);
        }
        Model.BasicPublish(_exchange, queue, null, MessagePackSerializer.Serialize(state)); 
    }

    public void StartInfill(string queue, ITargetBlock<State> target, StageReporting reporting, CancellationToken cancellationToken)
    {        
        if(!_existingQueues.Contains(queue)) {
            Model.QueueDeclare(queue, true, false, false, null);
            Model.QueueBind(queue, _exchange, queue); 
            _existingQueues.Add(queue);
        }
        
        var consumer = new EventingBasicConsumer(Model); 
        consumer.Received += (model, body) => {
            try {
                // Check out the MODEL class for possible variables to know if we're trying to cancel
                if (body.Body.IsEmpty) { 
                    return;
                }
                
                var state = MessagePackSerializer.Deserialize<State>(body.Body);
                switch (target.OfferMessage(new DataflowMessageHeader(_msgId++), state, null, false)) {
                    case DataflowMessageStatus.Accepted:
                        reporting.Processed++;
                        Model.BasicAck(body.DeliveryTag, false);
                        break;
                    case DataflowMessageStatus.DecliningPermanently:
                        Model.BasicNack(body.DeliveryTag, false, true);
                        Model.BasicCancel(body.ConsumerTag);
                        return;
                    default:
                        Model.BasicNack(body.DeliveryTag, false, true); 
                        reporting.Stall++;
                        Thread.Sleep(TimeSpan.FromSeconds(0.1));
                        break;
                } 
            } 
            catch (Exception ex) { 
                SentrySdk.CaptureException(ex); 
            }
        };   
        Model.BasicConsume(queue, false, reporting.Id.ToString(), consumer);
        _infillConsumers.Add(consumer);
    }
}