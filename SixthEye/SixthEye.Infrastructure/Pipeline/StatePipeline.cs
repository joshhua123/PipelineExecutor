﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using System.Threading.Tasks.Dataflow;
using Newtonsoft.Json;
using SixthEye.Infrastructure.Application;
using Sentry;
using SixthEye.Infrastructure.Pipeline.Source;
using SixthEye.Infrastructure.Pipeline.Target;

namespace SixthEye.Infrastructure.Pipeline
{
    public class StatePipeline : Pipeline<State>, ISelfReporting
    {
        private readonly List<object> _layoutReport = new ();
         
        public StatePipeline() { 
            _last_node = _first_buffer = new BufferBlock<State>(DefaultCapacitySettings);   
        }

        private IPropagatorBlock<State, TOut> Reporting<TOut>(IPropagatorBlock<State, TOut> stage, StageConfig config)
        {  
            var report = Services.ReportingRegister?.Get(config?.ReportingIdentifier); 
            _layoutReport.Add(new {report, config, stage.Completion.Id, buffer = stage});

            //var debugContent = stage.GetType().GetField("Content", BindingFlags.NonPublic | BindingFlags.Instance);
            
            //stage.AsObservable().Subscribe((n) => {
            //    // TODO This would be a good way to capture events and resend (State injection for debugging) 
            //    (n as State)?.variables.Add(config?.ReportingIdentifier ?? "", JsonConvert.SerializeObject(stage));
            //}, (ex) => { });
            
            return stage;
        }

        public void Then<TOut>(Func<State, TOut> func, StageConfig config)
        {
            var transformSettings = new ExecutionDataflowBlockOptions()
            {
                CancellationToken = CancellationTokenSource.Token,
                MaxDegreeOfParallelism = config.MaxConcurrency ?? 512,
                BoundedCapacity = config.MaxCapacity ?? 512
            };
            
            Install(Reporting(TransformWrap(func, config?.Identifier, transformSettings), config)); 
        }
        
        public void Then<TOut>(Func<State, Task<TOut>> func, StageConfig config) where TOut : Task
        {
            var transformSettings = new ExecutionDataflowBlockOptions()
            {
                CancellationToken = CancellationTokenSource.Token,
                MaxDegreeOfParallelism = config.MaxConcurrency ?? 512,
                BoundedCapacity = config.MaxCapacity ?? 512
            };
            
            var transformer = TransformWrapAsync(func, config?.Identifier, transformSettings);
            InstallAsync(Reporting(transformer, config)); 
        }

        public void ThenMany<TOut>(Func<State, TOut[]> func, StageConfig config)
        {
            var transformSettings = new ExecutionDataflowBlockOptions()
            {
                CancellationToken = CancellationTokenSource.Token,
                MaxDegreeOfParallelism = config.MaxConcurrency ?? 512,
                BoundedCapacity = config.MaxCapacity ?? 512
            };
            
            Install(Reporting(TransformWrap(func, config?.Identifier, transformSettings), config));
        }
        
        /*
        public void ThenMany<TOut>(Func<State, Task<TOut[]>> func, StageConfig config)
        {
            Install(Reporting(TransformWrap(func, config.Identifier), config));
        }
        */

        private void Then(Action<State> action, StageConfig config) { 
            var transformSettings = new ExecutionDataflowBlockOptions()
            {
                CancellationToken = CancellationTokenSource.Token,
                MaxDegreeOfParallelism = config.MaxConcurrency ?? 512,
                BoundedCapacity = config.MaxCapacity ?? 512
            };
            
            var block = new ActionBlock<State>(Wrap(action, config?.Identifier), transformSettings);
            LastNode.LinkTo(block); 
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public void Subscribe(IPipelineSource<State> source, StageConfig config)
        { 
            //var report = new StageReporting();
            //Services.Register?.Register(report);
            //config.ReportingIdentifier = report.ToString();
            _layoutReport.Add(new {config, report = source.Counters(), buffer = source});  
            source.Bind(_first_buffer);
        }

        public void Publish<T>(IPipelineTarget<T> target, Func<State, T> func, StageConfig config)
        { 
            //var report = new StageReporting();
            //Services.Register?.Register(report);
            _layoutReport.Add(new {config, report = target.Counters(), selfReport = target.SelfReport(), buffer = target});
            Then((State value) =>
            { 
                var result = func.Invoke(value);
                target.Send(result);
                //report.Processed++; 
            }, config); 
        }
        
        /*
        public void PublishMany<T>(IPipelineTarget target, Func<State, Task<T[]>> func,  StageConfig config)
        {
            // TODO This should break the return type to T, we'll need a ThenMany
            ThenMany((State value) =>
            {
                var awaiter = func.Invoke(value);
                awaiter.Wait(); // What to do about Async, there must be a better way, this won't scale
                var result = awaiter.Result;
                foreach (var item in result)
                    target.Send(item); 
                return result;
            }, config);
        }
        
        public void PublishMany<T>(IPipelineTarget target, Func<State, T[]> func, StageConfig config)
        {
            // TODO This should break the return type to T, we'll need a ThenMany
            ThenMany((State value) =>
            {
                var result = func.Invoke(value);  
                foreach (var item in result) 
                    target.Send(item); 
                return result;
            }, config);
        } 
        */

        public bool Feed(State value, StageConfig config)
        {
            return _first_buffer.Post(value);
        }

        public override string ToString()
        {
            return "encoded";
        }

        public object SelfReport()
        {
            return new { _layoutReport, counter = Counters(), _buffers = BufferBlocks.Select(x =>
            { 
                return x;
                // if (x is ActionBlock<State>) {
                //     return (x as ActionBlock<State>);
                // }
                // 
                // switch (x.GetType())
                // {
                //     case :
                //         break;
                //     
                //     case typeof(BufferBlock<State>):
                //         break;
                //     
                //     case typeof(TransformManyBlock<State, State>):
                //     case typeof(TransformBlock<State, State>):
                //         break;
                // }
            }),
                _linker = Linker };
        }

        public StageReporting Counters()
        { 
            return _reporting;
        }
    }
}