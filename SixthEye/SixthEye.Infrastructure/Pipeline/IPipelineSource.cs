﻿using System;
using System.Threading.Tasks.Dataflow;

namespace SixthEye.Infrastructure.Pipeline.Source
{
    public interface IPipelineSource<T>: ISelfReporting, IDisposable
    {
        void Bind(BufferBlock<T> targetBlock); 
    }
}