using System.Collections.Generic;

namespace SixthEye.Infrastructure.Pipeline
{
    public class StageConfig
    {
        public string Identifier { get; set; } = string.Empty;
        public string ReportingIdentifier { get; set; } = string.Empty;
        public string Module { get; set; } = string.Empty;
        public string Function { get; set; } = string.Empty;
        public int? MaxConcurrency { get; set; }
        public int? MaxCapacity { get; set; }

        public IDictionary<string, string> Parameters { get; set; } = null;
    }
}