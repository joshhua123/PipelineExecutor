using System;

namespace SixthEye.Infrastructure.Pipeline
{
    public class PipelineBuildException : Exception
    {
        public PipelineBuildException(string message) : base(message) {} 
    }
}