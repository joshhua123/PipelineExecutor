using System;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks.Dataflow;
using System.Timers;
using CsvHelper;
using CsvHelper.Configuration;
using SixthEye.Infrastructure.Application;
using Sentry;
using SixthEye.Infrastructure;
using SixthEye.Infrastructure.Pipeline;
using SixthEye.Infrastructure.Pipeline.Source;
using Timer = System.Timers.Timer;

namespace PipelineService.Feeders;

public class CsvSource : IPipelineSource<State>
{ 
    private readonly string _csvPath;
    private readonly TimeSpan _frequency;
    private readonly Timer _eventTrigger;
    private readonly StageReporting _report = new(); 
    private BufferBlock<State> _target;

    public CsvSource(string filePath, TimeSpan frequency)
    {
        _csvPath = filePath; 
        _frequency = frequency;
        _eventTrigger = new Timer(_frequency.TotalMilliseconds);
    }
 

    public void Bind(BufferBlock<State> targetBlock)
    {
        _target = targetBlock;
        long msgId = 1;
        
        Console.WriteLine("Source starting");

        async void Trigger(object sender, ElapsedEventArgs args) {
            try
            {
                await using var file = await Services.ObjectStorage.Get(_csvPath, "artifacts") as MemoryStream 
                                       ?? throw new Exception($"Unable to find csv file {_csvPath}");
                using var csv = new StringReader(Encoding.UTF8.GetString(file.ToArray()));
                using var reader = new CsvReader(csv, new CsvConfiguration(CultureInfo.InvariantCulture) { Delimiter = ",", HasHeaderRecord = true });

                await reader.ReadAsync();
                reader.ReadHeader();

                while (await reader.ReadAsync())
                {
                    try
                    {
                        retry:
                        var state = new State { variables = reader.HeaderRecord?.ToDictionary(x => x, x => reader.GetField(x)) };

                        var response =
                            (_target as ITargetBlock<State>).OfferMessage(new DataflowMessageHeader(msgId++), state, null, false);

                        switch (response)
                        {
                            case DataflowMessageStatus.DecliningPermanently:
                                return;
                            case DataflowMessageStatus.Accepted:
                                _report.AppendPeek(state);
                                _report.Processed++;
                                continue;
                            case DataflowMessageStatus.Declined:
                                Thread.Sleep(500);
                                goto retry; 
                        } 
                    }
                    catch (Exception ex)
                    {
                        _report.Fatal++;
                        SentrySdk.CaptureException(ex);
                    }
                }
            }
            catch (Exception ex)
            {
                SentrySdk.CaptureException(ex);
            }
        }

        _eventTrigger.Elapsed += Trigger;  
        Trigger(null, null);
        _eventTrigger.Start();  
    }

    public void Dispose()
    {
        //_csv?.Dispose();
        _eventTrigger?.Dispose();
    }

    public object SelfReport()
    {
        return new { _csvPath, _frequency };
    }

    public StageReporting Counters()
    {
        return _report;
    }
}