using System;
using System.Threading;
using System.Threading.Tasks;
using System.Threading.Tasks.Dataflow;
using SixthEye.Infrastructure.Application;
using Timer = System.Timers.Timer;

namespace SixthEye.Infrastructure.Pipeline.Source;
    public class WarehouseFeeder : IPipelineSource<State>, IDisposable
    {
        private string _partition;
        private TimeSpan _frequency;
        private ITargetBlock<State> _target; 
        private Timer _eventTrigger;
        private readonly StageReporting _report = new(); 
        private long id = 1;
        private IRecordStore _recordStore = Services.RecordStorage; 
        
        public WarehouseFeeder(string partition, TimeSpan frequency)
        {
            _partition = string.Intern(partition);
            _frequency = frequency; 
            _eventTrigger = new Timer(_frequency.TotalMilliseconds); 
        } 
 
        async Task Feeder()
        { 
            var backlog = Services.Backlog;
            foreach (var r in _recordStore.Iterate(_partition)) {
                try
                {
                    var state = new State { variables = r };
                    switch (_target.OfferMessage(new DataflowMessageHeader(id++), state, null, false)) 
                    {
                        case DataflowMessageStatus.Accepted:
                            _report.Processed++;
                            break;
                        case DataflowMessageStatus.DecliningPermanently:
                            return;
                        default:
                            _report.Stall++;
                            backlog.Send(_partition, state); 
                            Thread.Sleep(TimeSpan.FromSeconds(0.1));
                            break;
                    } 
                }
                catch (Exception ex) {
                    _report.Dropped++;
                    await Console.Error.WriteLineAsync(ex.Message);
                }
            }
        }
        
        void Start()
        { 
            Console.WriteLine("Source starting");

            _eventTrigger.Elapsed += (sender, args) =>
                Task.Factory.StartNew(async () => await Feeder()); 
            _eventTrigger.Start();

            // Start the process immediately.  
            Task.Factory.StartNew(async () => await Feeder()); 
        } 
        
        public void Bind(BufferBlock<State> targetBlock)
        {
            _target = targetBlock ?? throw new InvalidCastException($"{nameof(BufferBlock<State>)} can't be cast to {nameof(ITargetBlock<State>)}");
            Services.Backlog.StartInfill(_partition, _target, _report, CancellationToken.None);
            Start();  
        }

        public void Dispose()
        { 
        }

        public object SelfReport()
        {
            return new { _frequency, _partition };
        }

        public StageReporting Counters()
        {
            return _report;
        }
    }