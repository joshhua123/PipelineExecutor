using System;
using System.Collections.Concurrent;
using System.Threading;
using System.Threading.Tasks;
using System.Threading.Tasks.Dataflow;
using SixthEye.Infrastructure.Application;

namespace SixthEye.Infrastructure.Pipeline.Source;

public class InMemorySource : IPipelineSource<State>
{
    private readonly StageReporting _reporting = new(); 
    private readonly ConcurrentQueue<State> _queue;
    private readonly string _queueName;
    private Task _bindTask;
    
    public InMemorySource(string exchange, string queue) {
        _queue = InMemoryShared.GetOrCreateExchange(exchange).GetOrCreateQueue(queue);
        _queueName = queue;
    }
    
    public void Bind(BufferBlock<State> targetBlock) {
        Services.Backlog.StartInfill(_queueName, targetBlock, _reporting, CancellationToken.None);
        _bindTask = Task.Factory.StartNew(() => {
            var backlog = Services.Backlog;
            var target = targetBlock as ITargetBlock<State> ?? throw new InvalidCastException();
            long msgId = 1;
            while (target.Completion.IsCompleted == false)
            {
                if (_queue.TryDequeue(out var state)) {
                    var status = target.OfferMessage(new DataflowMessageHeader(msgId++),
                        state, null, false);

                    switch (status) {
                        case DataflowMessageStatus.Accepted:
                            _reporting.AppendPeek(state);
                            _reporting.Processed++; 
                            break;
                        default:
                            backlog.Send(_queueName, state);
                            _reporting.Stall++;
                            break; 
                    }
                } else {
                    Thread.Sleep(TimeSpan.FromMilliseconds(500));
                }
                
            }  
        }, TaskCreationOptions.LongRunning);
    }

    public object SelfReport() {
        return new { _queueName, _queue.Count};
    }

    public StageReporting Counters() => _reporting;

    public void Dispose() { 
        _bindTask?.Dispose();
    }
}