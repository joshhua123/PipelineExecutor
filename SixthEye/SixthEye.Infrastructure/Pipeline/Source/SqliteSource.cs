using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using System.Threading.Tasks.Dataflow;
using Microsoft.Data.Sqlite;
using Newtonsoft.Json;
using Sentry;
using SixthEye.Infrastructure.Application;

namespace SixthEye.Infrastructure.Pipeline.Source;

public class SqliteSource : IPipelineSource<State>
{
    private readonly StageReporting _reporting = new(); 

    private readonly SqliteConnection _memory; 
    private readonly string _exchange;
    private readonly string _queue;
    private Task _task;  
     
    public SqliteSource(string exchange, string queue)
    { 
        _memory = ConnectionFactory.GetConnection(":memory:"); 
        _exchange = exchange;
        _queue = queue;
        
        
        using var memCmd = _memory.CreateCommand();  
        memCmd.CommandText = $"CREATE TABLE IF NOT EXISTS {queue} (id varchar PRIMARY KEY, variables varchar) WITHOUT ROWID"; 
        memCmd.ExecuteNonQuery();    
    } 
    
    private bool Ack(string id)
    {
        // Since the memory connection is shared and disposing of a command is not a thread safe operation.
        var ackCommand = _memory.CreateCommand();
        ackCommand.CommandText = $"DELETE FROM {_queue} WHERE id=$id";
        ackCommand.Parameters.AddWithValue("id", id);
         
        var ack = ackCommand.ExecuteNonQuery() == 1; 
        lock(_memory)
            ackCommand.Dispose();
        return ack;
    }
    
    
    private Task ReaderTask(BufferBlock<State> targetBlock)
    {
        return Task.Factory.StartNew(() =>
        {
            var backlog = Services.Backlog;
            var target = targetBlock as ITargetBlock<State> ?? throw new InvalidCastException();
            long msgId = 1;
            
            //var cmdText = ; 
            using var command = _memory.CreateCommand();
            command.CommandText = $"SELECT id, variables FROM  {_queue}" ;
            
            ThreadLoop:
            var start = Stopwatch.StartNew();
            try
            {

                // this function needs to repeat, and the id's from read state are then batch deleted 
                using var reader = command.ExecuteReader();
                while (reader.HasRows && reader.Read())
                {
                    var cycle = Stopwatch.StartNew();

                    try
                    {
                        // Deletion acts as ACK
                        var stateId = reader.GetString(0);
                        if (Ack(stateId))
                        {
                            var state = new State()
                            {
                                Id = stateId,
                                variables =
                                    JsonConvert.DeserializeObject<IDictionary<string, string>>(reader.GetString(1))
                            };
                            var status = target.OfferMessage(new DataflowMessageHeader(msgId++),
                                    state, null, false);

                            switch (status)
                            {
                                case DataflowMessageStatus.Accepted:
                                    _reporting.AppendPeek(state);
                                    _reporting.Processed++;

                                    break;
                                default:
                                    backlog.Send(_queue, state);
                                    _reporting.Stall++;
                                    break;

                            }
                        }
                        else
                        {
                            _reporting.Dropped++;
                        }

                    }
                    catch (Exception ex)
                    {
                        SentrySdk.CaptureException(ex);
                    }

                    cycle.Stop();
                    Services.Metrics.Report(_queue, "ack", new { Duration = cycle.ElapsedMilliseconds });
                }
            }
            catch (Exception ex)
            {
                SentrySdk.CaptureException(ex);
            }

            start.Stop();

            // Throttle mechanism
            if (start.ElapsedMilliseconds < 50)
                Thread.Sleep(TimeSpan.FromSeconds(1));

            goto ThreadLoop;
        }, TaskCreationOptions.LongRunning); 
    }
    
    
    public void Bind(BufferBlock<State> target)
    {
        Services.Backlog.StartInfill(_queue, target, _reporting, CancellationToken.None);
        _task = ReaderTask(target);
    }

    public object SelfReport() {
        return new { status = _task.Status, error = _task.Exception };
    }

    public StageReporting Counters() { return _reporting; }

    public void Dispose() { 
        _memory?.Dispose(); 
        _task?.Dispose(); 
    }
}