﻿using System;
using System.Buffers;
using System.Collections.Generic; 
using System.Text; 
using System.Threading;
using System.Threading.Tasks;
using System.Threading.Tasks.Dataflow;
using MessagePack;
using Newtonsoft.Json;
using RabbitMQ.Client;
using SixthEye.Infrastructure.Application; 
using RabbitMQ.Client.Events;
using Sentry;  

namespace SixthEye.Infrastructure.Pipeline.Source;

public class RabbitSource : IPipelineSource<State>
{  
    private readonly string _queue;
    private readonly string _exchange; 
    private long _messageCount = 1;

    private IModel Model { get ; }
    private readonly IConnection _connection;
    
    private ITargetBlock<State> _target;  
    private readonly StageReporting _reporting = new(); 
 
    private EventingBasicConsumer _consumer; 
    private string _consumerTag = String.Empty;
    
    private Stack<State> debug = new ();
          
    public RabbitSource(string exchange, string queue)
    {
        _exchange = exchange;
        _queue = queue;
        
        _connection = Services.RabbitMqFactory.CreateConnection();
        _exchange = exchange;
        _queue = queue; 
        Model = _connection.CreateModel();  
        
        Model.ExchangeDeclare(exchange, "direct", true);
        // Create queue
        Model.QueueDeclare(queue, true, false, false, null);  
    }

    private bool AttemptForward(ReadOnlyMemory<byte> message)
    {
        try
        {
        
            if (((_target as BufferBlock<State>)?.Count ?? 0) >= 128)
                return false;
                
            // TODO Here we deserialize all messages even once at capacity. 
            // Ideally we should predict if status will fail and lazy deserialize
            var value = MessagePackSerializer.Deserialize<State>(message);
        
            if (value is null) {
                _reporting.Dropped++;
                return true;
            }
        
            var status = _target.OfferMessage(new DataflowMessageHeader(_messageCount++),
                value, null, false);
        
            return status == DataflowMessageStatus.Accepted; 
        
        }
        catch (Exception ex) {
            SentrySdk.CaptureException(ex);
            return false;
        }
    }
        
    public void Bind(BufferBlock<State> targetBlock)
    {
        _target = targetBlock;
        
        _consumer = new EventingBasicConsumer(Model); 
        _consumer.Received += (model, body) => {
            try
            {
                // Check out the MODEL class for possible variables to know if we're trying to cancel
                if (body.Body.IsEmpty) { 
                    return;
                }

                
                if(AttemptForward(body.Body))
                    Model.BasicAck(body.DeliveryTag, false);
                else 
                    Model.BasicNack(body.DeliveryTag, false, true); 
            } 
            catch (Exception ex) { 
                SentrySdk.CaptureException(ex);
            }
        }; 

        Start();
    } 
    
    private void Start() { 
        lock (_consumerTag) {
            if (string.IsNullOrEmpty(_consumerTag)) {
                _consumerTag = Guid.NewGuid().ToString();
                Model.BasicConsume(_queue, false, _consumerTag, _consumer); 
            }    
        }
    }
    
    private void Cancel() {
        lock (_consumerTag) {
            if (string.IsNullOrWhiteSpace(_consumerTag)) {
                Model.BasicCancel(_consumerTag); 
                _consumerTag = string.Empty;
            }    
        }
    }

    public string Queue
    {
        get => _queue;
    }

    public object SelfReport()
    {
        return new
        {
            queue = Queue,
            exchange = _exchange,
            id = _target?.Completion?.Id, 
        };
    }

    public StageReporting Counters()
    {
        return _reporting;
    }

    public void Dispose()
    {
        _connection?.Dispose();
        Model?.Dispose();
    }
}