using System.Threading.Tasks.Dataflow;
using SixthEye.Infrastructure.Application;
using SixthEye.Infrastructure.Pipeline.Source;

namespace SixthEye.Infrastructure.Pipeline.ZeroMq;

public class ZeroMqSource : IPipelineSource<State>
{
    public void Bind(BufferBlock<State> targetBlock)
    {
        throw new System.NotImplementedException();
    }

    public object SelfReport()
    {
        throw new System.NotImplementedException();
    }

    public StageReporting Counters()
    {
        throw new System.NotImplementedException();
    }

    public void Dispose()
    {
    }
}