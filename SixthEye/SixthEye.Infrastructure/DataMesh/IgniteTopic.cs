namespace SixthEye.Infrastructure.DataMesh
{
    public class IgniteTopic : ITopic
    {
        public string TopicString { get; private set; }
         
        public static ITopic Create(string topic) => new IgniteTopic {TopicString = topic};
         
    }
}