using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq; 
using System.Reactive.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks; 
using Minio; 

namespace SixthEye.Infrastructure.ObjectStorage
{
    public class MinIOStorage : IObjectStorage
    {
        private readonly MinioClient _minioClient;
        private readonly string _bucket;

        public MinIOStorage(string bucket)
        {
            _minioClient = new MinioClient()
                .WithEndpoint($"{Services.ServerHost}:9000")
                .WithCredentials("accessKey", "secretKey")
                .Build(); 
            _bucket = bucket;
        }

        private string GetPath(string folder, string objectName) => (folder == null ? "" : folder + "/") + objectName;
        
        public async Task<Stream> Get(string objectName, string folder = null)
        {
            var response = new MemoryStream();

            var stat = await _minioClient.GetObjectAsync(new GetObjectArgs()
                .WithBucket(_bucket)
                .WithObject(GetPath(folder, objectName))
                .WithCallbackStream(s => s.CopyTo(response)));

            response.Seek(0, SeekOrigin.Begin);
            return response;
        }
        
        public async Task Put(Stream data, string objectName, string folder = null) {   
            await _minioClient.PutObjectAsync(new PutObjectArgs()
                .WithBucket(_bucket)
                .WithObject(GetPath(folder, objectName))
                .WithObjectSize(data.Length)
                .WithStreamData(data)
                .WithContentType("application/octet-stream")
            );
        }

        public Task<List<string>> List(string folder = null) {
            var objects = new ConcurrentStack<string>(); 
            var observable = _minioClient.ListObjectsAsync(new ListObjectsArgs() 
                .WithRecursive(true)
                .WithPrefix(folder)
                .WithBucket(_bucket));   
            // TODO I don't like this solution since we retrieve all files in a folder too,
            // then deduplicate each file's occurence to only have one copy of the folder/appname
            using var subscription = observable.Subscribe(x => objects.Push(x.Key.Split('/')[1])); 
            Thread.Sleep(TimeSpan.FromMilliseconds(200));
            observable.Wait();
            observable.Retry();
            if (objects.IsEmpty)
                return Task.FromResult(new List<string>(Array.Empty<string>()));
            return Task.FromResult(objects.Distinct().ToList());
        }

        public async Task Delete(string objectName, string folder = null) {
            await _minioClient.RemoveObjectAsync(new RemoveObjectArgs().WithBucket(_bucket)
                .WithObject(GetPath(folder, objectName))); 
        }
    }
}