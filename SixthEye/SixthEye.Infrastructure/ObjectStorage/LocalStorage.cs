using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace SixthEye.Infrastructure.ObjectStorage;

public class LocalStorage : IObjectStorage
{
    private readonly string _root;
    public LocalStorage(string root)  {
        _root = root ?? Environment.CurrentDirectory;
    }
    
    private string GetPath(string folder, string file) =>
        Path.Combine(_root, folder is null ? file : Path.Combine(folder, file));
    
    public Task<Stream> Get(string objectName, string folder = null) =>
        Task.FromResult(File.OpenRead(GetPath(folder, objectName)) as Stream);
    

    public async Task Put(Stream data, string objectName, string folder = null)
    {
        await using var dst = File.OpenWrite(GetPath(folder, objectName));
        await data.CopyToAsync(dst);
    }

    public Task<List<string>> List(string folder = null)
    {
        var path = Path.Combine(_root, folder ?? string.Empty);
        var result = Directory.EnumerateFiles(path)
            .Concat(Directory.EnumerateDirectories(path))
            .ToList();
        return Task.FromResult(result);
    }

    public Task Delete(string objectName, string folder = null){
        File.Delete(GetPath(folder, objectName));
        return Task.CompletedTask;
    }
}