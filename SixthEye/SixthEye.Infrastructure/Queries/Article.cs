﻿using System;
using System.Data;
using System.Data.Common;
using System.Globalization;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using Microsoft.Extensions.Logging;
using Sentry;
using Sentry.Protocol;

namespace SixthEye.Infrastructure.Queries
{
    // TODO 
    // 1. Find out what this design pattern is called
    // 2. Try to implement a base class that uses reflection to construct the Update/Insert/Get queries and handles
    //    FilterIndex like isScraped that has a bit attached to the id index. This gives us fast lookups
    //    HashIndex like Url and Url_Hash, This will store a long and allow index lookups.
    // I'd like to automate the creation of these indexes.
    // We might have to worry about version control, but preferably not.
    
    public class Article : Core.PersistentObjects.Article
    { 
        public bool Exists { get; } = true; 

        public static string SqlStringToHash = "CONVERT(BIGINT, CONVERT(VARBINARY(8), HashBytes('SHA2_256', @Url), 1))";
        
        private static string UpdateSQL = "UPDATE dbo.Articles SET domain=@domain, title=@title, url=@url, url_hash=@url_hash, publicated=@publicated, found=@found, search_source=@search_source, summary=@summary, content=@content, scraped=@scraped, isScraped=@isScraped, Subscriptionid=@SubscriptionId WHERE id=@id";
        private static string InsertSQL = "INSERT INTO dbo.Articles (domain, title, url, url_hash, publicated, found, search_source, summary, content, scraped, isScraped, Subscriptionid) VALUES (@domain, @title, @url, @url_hash, @publicated, @found, @search_source, @summary, @content, @scraped, @isScraped, @Subscriptionid)";
        
        private static string GetSQLWithId = "SELECT * FROM dbo.Articles WHERE id = @id";
        private static string GetSQLWithHash = "SELECT * FROM dbo.Articles WHERE url_hash = @hash";

        public static long ComputeHash(string url)
        {                
            using (var sha256 = new SHA256Managed())
            {
                var hex = BitConverter.ToString(sha256.ComputeHash(Encoding.ASCII.GetBytes(url))).Replace("-", "");
                return long.Parse(hex.AsSpan(0, 16), NumberStyles.HexNumber); 
            } 
        }
        
        public long Hash
        {
            get => ComputeHash(url); 
        } 
        
        public Article(IDbConnection connection, int id) {
            try{
                var article = connection.QueryFirstOrDefault<Core.PersistentObjects.Article>(GetSQLWithId, new { id });

                if (article == null) {
                    Exists = false;
                    return; 
                } 

                this.id = article.id;
                this.domain = article.domain;
                this.title = article.title;
                this.url = article.url;
                this.url_hash = ComputeHash(this.url);
                this.publicated = article.publicated;
                this.found = article.found;
                this.search_source = article.search_source;
                this.summary = article.summary;
                this.content = article.content;
                this.scraped = article.scraped;
                this.SubscriptionId = article.SubscriptionId;
            }
            catch (Exception ex)
            {
                Services.Log<Article>().LogError(ex.Message, ex); 
            }
        }
        
        public Article(IDbConnection connection, string url) {
            try
            { 
                var article = connection.QueryFirstOrDefault<Core.PersistentObjects.Article>(GetSQLWithHash, new {hash = ComputeHash(url)});

                if (article == null)
                {
                    Exists = false;
                    return;
                }

                this.id = article.id;
                this.domain = article.domain;
                this.title = article.title;
                this.url = article.url;
                this.url_hash = ComputeHash(this.url);
                this.publicated = article.publicated;
                this.found = article.found;
                this.search_source = article.search_source;
                this.summary = article.summary;
                this.content = article.content;
                this.scraped = article.scraped;
                this.SubscriptionId = article.SubscriptionId;
            }
            catch (Exception ex)
            {
                Services.Log<Article>().LogError(ex.Message, ex); 
            }
        }

        /// <summary>
        /// Inserts the article into the database
        /// </summary>
        /// <param name="connection"></param> 
        public void Commit(IDbConnection connection) 
        {
            try{
                this.url_hash = Hash;
                this.isScraped = scraped != null; 
                
                connection.Execute(Exists ? UpdateSQL : InsertSQL,this); 
            }
            catch (Exception ex) {
                Services.Log<Article>().LogError(ex.Message, ex); 
            }  
        }
        
        
        /// <summary>
        /// Inserts the article into the database
        /// </summary>
        /// <param name="connection"></param> 
        public async Task CommitAsync(IDbConnection connection) 
        {
            try{
                // Computed variables
                this.url_hash = Hash;
                this.isScraped = scraped != null;
 
                await connection.ExecuteAsync(Exists ? UpdateSQL : InsertSQL,this); 
            }
            catch (Exception ex) {
                Services.Log<Article>().LogError(ex.Message, ex); 
            }
        }
    }
}