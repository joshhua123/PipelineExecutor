using Microsoft.AspNetCore.Hosting; 
using Microsoft.Extensions.Hosting; 
using SixthEye.Infrastructure;

namespace PipelineService
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args) 
            .ConfigureWebHostDefaults(webBuilder =>
            {
                // Add the following line:
                //webBuilder.UseSentry();
                webBuilder.UseSentry(o =>
                {
                    o.Dsn = "http://5322e2b3f1a44bf2823da707115acdfd@10.0.0.102:9000/2";
                    // When configuring for the first time, to see what the SDK is doing:
                    o.Debug = true;
                    o.Release = "development";
                    o.AttachStacktrace = true;
                    // Set traces_sample_rate to 1.0 to capture 100% of transactions for performance monitoring.
                    // We recommend adjusting this value in production.
                    o.TracesSampleRate = 1.0;
                    o.MaxQueueItems = 100;
                }); 
                webBuilder.UseStartup<Startup>();
            });
    }
}