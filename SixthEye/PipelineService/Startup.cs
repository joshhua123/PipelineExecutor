using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting; 
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting; 
using Sentry; 

namespace PipelineService
{
    public class Startup
    {
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        { 
            services.AddControllers();
               
            services.AddSentry();
            
            services.AddCors(options =>
            {
                options.AddDefaultPolicy(
                    builder =>
                    { 
                        builder.AllowAnyOrigin();
                        builder.AllowAnyHeader();
                        builder.AllowAnyMethod();
                        builder.SetPreflightMaxAge(TimeSpan.FromMinutes(60));
                        //builder.WithHeaders(new[] { "*" });
                    });
            });

            SentrySdk.Init(new SentryOptions() {
                Dsn = "http://33a7aaccb76711ec95670242ac120019@10.0.0.102:9000/2",
                Debug = false,
                DeduplicateMode = DeduplicateMode.SameExceptionInstance
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();  
            }
 
            app.UseCors();
            app.UseRouting();  
            app.UseSentryTracing(); 
            
            app.UseEndpoints(endpoints => {
                endpoints.MapControllers(); 
            });
        }
    }
}