﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using InfluxDB.Client.Api.Domain;
using Microsoft.AspNetCore.Mvc;
using SixthEye.Infrastructure;

namespace PipelineService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CompileController : ControllerBase
    {
        private readonly string _templatePath = @"../wasmodule";
        
        private void Compile()
        {
            System.Diagnostics.Process process = new System.Diagnostics.Process();  
            //process.StartInfo = new System.Diagnostics.ProcessStartInfo { 
            //    WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden,
            //    WorkingDirectory = _templatePath,
            //    FileName = "cargo",
            //    Arguments = "build --release --target wasm32-unknown-unknown"
            //}; 
            process.StartInfo = new System.Diagnostics.ProcessStartInfo { 
                WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden,
                WorkingDirectory = _templatePath,
                FileName = "npm",
                Arguments = "run asbuild"
            };
            process.Start();
            process.WaitForExit(); 
        }
        
        
        [HttpGet("{module}")]
        public async Task<ActionResult> Compile(string module)
        {
            using var source = await Services.ObjectStorage.Get("index.ts", $"modules/{module}") as MemoryStream
                         ?? throw new Exception("Invalid Stream for Compile");
              
            System.IO.File.WriteAllBytes(_templatePath + "/assembly/index.ts", source.ToArray());
 
            System.Diagnostics.Process process = new System.Diagnostics.Process();   
            process.StartInfo = new System.Diagnostics.ProcessStartInfo { 
                WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden,
                WorkingDirectory = _templatePath,
                FileName = "npm",
                RedirectStandardError = true,
                Arguments = "run asbuild"
            };
            process.Start(); 
            await process.WaitForExitAsync();  
            await Services.ObjectStorage.Put(System.IO.File.OpenRead(_templatePath + "/build/release.wasm"),
                "run.wasm", $"modules/{module}");
            return Ok(process.StandardError.ReadToEnd());
        }
    }
}