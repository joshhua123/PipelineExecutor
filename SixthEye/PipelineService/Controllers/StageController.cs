using System;
using MessagePack;
using Microsoft.AspNetCore.Cors.Infrastructure;
using Microsoft.AspNetCore.Http.HttpResults;
using Microsoft.AspNetCore.Mvc;
using SixthEye.Infrastructure;
using SixthEye.Infrastructure.Pipeline;

namespace PipelineService.Controllers
{
    
    [Route("api/stage")]
    [ApiController]
    public class  StageController : ControllerBase
    {
        private readonly StageReportingRegister _reportingRegister;
        
        public StageController() {
            _reportingRegister = Services.ReportingRegister;
        }

        
        // GET: api/Stage/5
        [HttpGet("{id}", Name = "StageGet")]
        public ActionResult Get(string id)
        {
            switch (id)
            { 
                case null:
                    return new NotFoundResult();
                default: 
                    var stageReporting = _reportingRegister.Get(id);  
                    return stageReporting is null ? new NotFoundResult() : new ObjectResult(MessagePackSerializer.SerializeToJson(stageReporting));
            } 
        }  
    }
}