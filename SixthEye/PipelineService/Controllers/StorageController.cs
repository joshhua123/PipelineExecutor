﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNetCore.Mvc; 
using SixthEye.Infrastructure;

namespace PipelineService.Controllers;

[Route("api/[controller]")]
[ApiController]
public class StorageController : ControllerBase 
{
    // GET: api/storage/
    [HttpGet("{fileName}")]
    public async Task<ActionResult> Get(string fileName)
    {
        using var stream = await Services.ObjectStorage.Get(HttpUtility.UrlDecode(fileName)) as MemoryStream;
        return new ContentResult{Content = Encoding.UTF8.GetString(stream.ToArray())};
    }
    
    // GET: api/storage/
    [HttpPost("{fileName}")]
    public async Task<ActionResult> Put(string fileName)
    {
        using var memory = new MemoryStream();
        await Request.Body.CopyToAsync(memory);
        memory.Seek(0, SeekOrigin.Begin);
        await Services.ObjectStorage.Put(memory, HttpUtility.UrlDecode(fileName));
        return Ok();
    }
}