using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc; 
using SixthEye.Infrastructure;

namespace PipelineService.Controllers;

[Route("api/[controller]")]
[ApiController]
public class ApplicationController : Controller
{   
    [HttpGet("status/{appName}")]
    public async Task<IActionResult> Status(string appName) {
        if(Services.Library.Running.TryGetValue(appName, out var instance))
            return new JsonResult( new { 
                instance.ApplicationName,
                Actors = instance.Actors.Select(x => x.SelfReport())
            });
        return NotFound();
    }
    
        
    [HttpGet("list")]
    public async Task<IActionResult> List() {
        return new JsonResult(await Services.Library.List());
    }
    
    [HttpGet("running")]
    public IActionResult Running() {
        return new JsonResult(Services.Library.Running.Keys);
    }
    
    [HttpGet("start/{appName}")]
    public async Task<IActionResult> Start(string appName) {
        await Services.Library.Start(appName);
        return Ok();
    }   
    
    [HttpGet("stop/{appName}")]
    public IActionResult Stop(string appName) {
        Services.Library.Stop(appName);
        return Ok();
    }
}