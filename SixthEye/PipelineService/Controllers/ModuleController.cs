using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http.Json;
using System.Text;
using System.Threading.Tasks;
using AngleSharp.Html.Dom.Events;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using PipelineService.Plugins;
using SixthEye.Infrastructure;
using SixthEye.Infrastructure.Application;
using SixthEye.Infrastructure.Executor;

namespace PipelineService.Controllers;

[Route("api/module")]
[ApiController]
public class ModuleController : ControllerBase
{
    [HttpGet("list")]
    public async Task<ActionResult> List()
    {
        var modules = await Services.ObjectStorage.List("modules/"); 
        return new JsonResult(modules);
    }

    // Uploads a wasm runtime to the module
    [HttpPost("{module}/upload")]
    public async Task<ActionResult> Upload(string module)
    {
        var stream = new MemoryStream();
        await Request.Body.CopyToAsync(stream);
        stream.Seek(0, SeekOrigin.Begin);
        await Services.ObjectStorage.Put(stream, "run.wasm", $"modules/{module}");
        return new OkResult();
    }

    /// <summary>
    /// Returns the wasm module metadata. function exports and imports
    /// </summary>
    /// <returns></returns>
    [HttpGet("{module}/meta")]
    public async Task<ActionResult> Meta(string module)
    {
        try
        {
            var bufferTask = await
                Services.ObjectStorage.Get($"run.wasm", "modules/" + module) as MemoryStream;
            //var instance = new WasmtimeExecutor(bufferTask.ToArray(), module);
            return new JsonResult(Array.Empty<string>());
            //return new JsonResult(instance.Functions()); 
        } catch(Exception ex) {
            return NotFound(); 
        } 
    }

    [HttpGet("{module}/create")]
    public async Task<ActionResult> Create(string module) {
        await using var template = System.IO.File.OpenRead("/home/joshua/Git/wasmodule/assembly/index.ts");
        await Services.ObjectStorage.Put(template, "index.ts", $"modules/{module}");
        return new OkResult();
    }
    
    [HttpGet("{module}/{function}/test")]
    public async Task<ActionResult> Test(string module, string function)
    {
        return new OkResult();
        /*
        var config = new Dictionary<string, string>() {{ "module", module }}; 
        var wasm = new Wasm();
        wasm.Init(function, config);

        State state; 
        try
        {
            var sample = await Services.ObjectStorage.Get("sample.json", $"modules/{module}") as MemoryStream;
            state = sample == null ? new State() : JsonConvert.DeserializeObject<State>(Encoding.UTF8.GetString(sample.ToArray()));
        } catch (Exception ex) {
            state = new State();
        }

        return new JsonResult(await  wasm.Run(state)); 
        */
    }
    
    [HttpGet("{module}")]
    public async Task<ActionResult> Get(string module) { 
        var stream = await Services.ObjectStorage.Get("index.ts", $"modules/{module}") as MemoryStream
            ?? throw new NotImplementedException("ModuleController expects MemoryStream");
        return new ContentResult() { Content = Encoding.UTF8.GetString(stream.ToArray()) };
    }     
    [HttpGet("{module}/sample")]
    public async Task<ActionResult> GetSample(string module) { 
        var stream = await Services.ObjectStorage.Get("sample.json", $"modules/{module}") as MemoryStream
                     ?? throw new NotImplementedException("ModuleController expects MemoryStream");
        return new ContentResult() { Content = Encoding.UTF8.GetString(stream.ToArray()) };
    } 
    
    [HttpPut("{module}")]
    public async Task<ActionResult> Put(string module)
    {
        var stream = new MemoryStream();
        await Request.Body.CopyToAsync(stream);
        stream.Seek(0, SeekOrigin.Begin); 
        await Services.ObjectStorage.Put(stream, "index.ts", $"modules/{module}");
        return new OkResult();
    } 
    
    [HttpPut("{module}/sample")]
    public async Task<ActionResult> PutSample(string module)
    {
        var stream = new MemoryStream();
        await Request.Body.CopyToAsync(stream);
        stream.Seek(0, SeekOrigin.Begin); 
        await Services.ObjectStorage.Put(stream, "sample.json", $"modules/{module}");
        return new OkResult();
    } 
}