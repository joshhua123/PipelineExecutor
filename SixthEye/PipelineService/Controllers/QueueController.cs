using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Sentry;
using SixthEye.Infrastructure;
using SixthEye.Infrastructure.Pipeline;

namespace PipelineService.Controllers;

[Route("api/queue")]
[ApiController]
public class QueueController  : ControllerBase
{

    [HttpGet("{queue}")]
    public ActionResult Get(string queue)
    {
        var connection = ConnectionFactory.GetConnection(":memory:");
        var backlogConn = ConnectionFactory.GetConnection($"backlog_v2.db");
        
        
        long? queueCount = null;
        try
        {
            using var command = connection.CreateCommand();
            command.CommandText = $"SELECT COUNT(*) FROM {queue}";
            queueCount = command.ExecuteScalar() as long?;
        } catch (Exception ex) {
            SentrySdk.CaptureException(ex);
        }

        long? backlogCount = null;
        try {
            using var backlogCmd = backlogConn.CreateCommand();
            backlogCmd.CommandText = $"SELECT COUNT(*) FROM {queue}";
            backlogCount = backlogCmd.ExecuteScalar() as long?;
        }
        catch (Exception ex)
        {
            SentrySdk.CaptureException(ex);
        }

        return new JsonResult(new {queueCount, backlogCount});
    }
    
    
    [HttpGet("")]
    public ActionResult List()
    {
        var connection = ConnectionFactory.GetConnection(":memory:");
        var backlogConn = ConnectionFactory.GetConnection($"backlog_v2.db");
          
        using var command = connection.CreateCommand(); 
        command.CommandText = $"SELECT name FROM sqlite_master WHERE type ='table' AND name NOT LIKE 'sqlite_%';";
        using var memoryReader = command.ExecuteReader();
        
        using var backlogCmd = backlogConn.CreateCommand(); 
        backlogCmd.CommandText = $"SELECT name FROM sqlite_master WHERE type ='table' AND name NOT LIKE 'sqlite_%';";
        using var backlogReader = backlogCmd.ExecuteReader();

        var queueNames = new List<string>();
        while (backlogReader.Read()) {
            queueNames.Add(backlogReader.GetString(0));
        }
        while (memoryReader.Read()) {
            queueNames.Add(memoryReader.GetString(0));
        }
        
        return new JsonResult(queueNames.Distinct());
    }

}