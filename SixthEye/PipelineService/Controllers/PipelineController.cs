using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SixthEye.Infrastructure.Application;
using SixthEye.Infrastructure;

namespace PipelineService.Controllers
{
    [Route("api/pipeline")]
    [ApiController]
    public class PipelineController : ControllerBase
    {  
        // GET: api/Pipeline/5
        [HttpGet("list")]
        public ActionResult List() { 
            return new JsonResult(Services.Library.Running.SelectMany(x => x.Value.Actors.Select(y => y.Pipeline.SelfReport()))); 
        } 
        
        [HttpGet("{id:guid}")]
        public ActionResult Get(Guid id) { 
            return new JsonResult(Services.Library.Running
                .SelectMany(x => x.Value.Actors)
                .First(x => x.id.Equals(id))
                .SelfReport()); 
        } 
        
        [HttpGet("{id:guid}/stop")]
        public ActionResult Stop(Guid id)
        {
            var pipeline = Services.Library.Running
                .SelectMany(x => x.Value.Actors)
                .First(x => x.id.Equals(id));
            pipeline.Pipeline.CancellationTokenSource.Cancel();
            pipeline.Dispose();
            return Ok();
        }  
    }
}