﻿using System.Text.Json;

namespace SixthEye.Infrastructure.Application
{
    public class MessageRequest
    {
        public string operation { get; set; }
        public string id { get; set; }
        public string stage { get; set; }
        public JsonElement body { get; set; }
    }
}