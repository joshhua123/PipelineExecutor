namespace SixthEye.Infrastructure.Application
{
    public class LaunchRequestBody : IMessageBody
    {
        public string name { get; set; }  
    }
}