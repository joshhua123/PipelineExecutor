﻿using System.Collections.Generic;

namespace SixthEye.Infrastructure.Application
{
    public class ThenRequestBody : IMessageBody
    { 
        public string module { get; set; } 
        public string version { get; set; }
        public string function { get; set; }
        public IDictionary<string, string> parameters { get; set; }
    }
}