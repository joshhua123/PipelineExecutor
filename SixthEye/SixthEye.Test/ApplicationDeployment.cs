using System.Linq;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;  
using SixthEye.Infrastructure.Application; 
using Node = System.Collections.Generic.IDictionary<object, object>;

namespace SixthEye.Test;

[TestClass]
public class ApplicationDeployer
{  
 
    // Test requires data to be setup in the minio 
    [TestMethod]
    public async Task YamlLaunch()
    {
        IAppLauncher launcher = new ObjectStoreLauncher(); 
        Assert.AreNotEqual(0, (await launcher.Launch("NewsApp")).Count());
    } 
}