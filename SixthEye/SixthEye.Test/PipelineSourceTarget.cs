using System;
using System.Collections.Generic;
using System.Threading; 
using Microsoft.VisualStudio.TestTools.UnitTesting; 
using SixthEye.Infrastructure.Application;
using SixthEye.Infrastructure.Pipeline;
using SixthEye.Infrastructure.Pipeline.Source;
using SixthEye.Infrastructure.Pipeline.Target;

namespace SixthEye.Test;

[TestClass]
public class MessagingSubscription
{
    private IPipelineSource<State> _subscription;
    private IPipelineTarget<State> _publisher;

    [TestInitialize]
    public void Setup()
    {
        _subscription = new InMemorySource("test", nameof(MessagingSubscription));
        _publisher = new InMemoryTarget("test", nameof(MessagingSubscription));
    }

    /// <summary>
    /// If this test fails, check that publishing is working as that functionality is required to send a message to receive
    /// </summary>
    [TestMethod]
    public void Subscribe()
    {
        bool received = false;

        var state = new StatePipeline();
        state.Subscribe(_subscription, new StageConfig()); 
        state.Then((State s) => {
            s.variables.TryGetValue("test", out var name);
            Assert.IsTrue(string.Compare(nameof(Subscribe), name) == 0);
            received = true;
            return s;
        }, new StageConfig()); 
        
        _publisher.Send(new State(){variables = new Dictionary<string, string>(){{"test", nameof(Subscribe)}}});
        SpinWait.SpinUntil(() => received, TimeSpan.FromSeconds(15));
        Assert.IsTrue(received);
    }
}