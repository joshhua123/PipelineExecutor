using System;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SixthEye.Infrastructure.Application;
using SixthEye.Infrastructure.RecordStore.Impl;

namespace SixthEye.Test;

[TestClass]
public class StateStore
{
    private readonly IRecordStore _store = new NpgsqlRecord();
    
    [TestMethod]
    public async Task Put() {
        await _store.Put(new State(){variables = {{"value", "test"}}}, "test", new []{"value"});
    }
    
    [TestMethod]
    public async Task Get() {
        await _store.Get("test");
    }    
    
    [TestMethod]
    public async Task Contains() {
        await _store.Put(new State(){variables = {{"value", "true"}}}, "test", new []{"value"});
        Assert.IsTrue(await _store.Contains("value", "true", "test"));
        Assert.IsFalse(await _store.Contains("value", "false", "test"));
    }
}