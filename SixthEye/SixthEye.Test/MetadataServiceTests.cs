﻿using System.Threading.Tasks;
using Google.Protobuf.WellKnownTypes;
using Grpc.Core;   
using Moq;
using SixthEye.Infrastructure;
using SixthEye.RPCManager.Services;
using Xunit; 

namespace SixthEye.Test
{ 
    public class MetadataServiceTests
    { 
        [Fact]
        public async Task RequestMetadata()
        {
            using var connection = Services.Database;
              
            var mockContext = new Mock<ServerCallContext>(); 
            
            var service = new ContentService(null, connection);  
 
            var results = await service.RequestMetadata(new Empty(), mockContext.Object);
 
            Assert.NotNull(results);
            Assert.NotEmpty(results.Metadata);
        }
    }
}
