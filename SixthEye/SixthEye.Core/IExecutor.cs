﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using SixthEye.Infrastructure.Application;
using SixthEye.Infrastructure.Pipeline;

namespace SixthEye.Infrastructure.Executor
{ 
    public interface IExecutor : IDisposable, ISelfReporting
    { 
        public Task<State[]> Run(State state);
        public void Init(string function, IDictionary<string, string> parameters);
        /// <summary>
        /// Returns a list of function/parameter pairs that this executor supports
        /// </summary>
        /// <returns></returns>
        public IDictionary<string, IEnumerable<string>> Functions();
    }
}
