using System;
using System.Collections.Generic;
using System.Linq; 
using SixthEye.Infrastructure.Executor; 

namespace PipelineService.Plugins
{
    public interface IPlugin : IExecutor
    { 
        /**
         * Function: string to signal which function the plugin should use
         * State: incoming state 
         * Parameters: parameters supplied by the pipeline
         */   
        
        public static IDictionary<string, string> ValidateParameters(string[] required, IDictionary<string, string> parameters)
        {
            var x = required.Where(x => !parameters.ContainsKey(x)).ToArray();
            if (x.Any())
                throw new Exception($"Missing Parameter: {string.Join(", ", x)}");
            
            return parameters;
        } 

    } 
}