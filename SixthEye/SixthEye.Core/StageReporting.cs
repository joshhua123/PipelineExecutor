using System;
using System.Collections.Concurrent; 
using MessagePack;
using SixthEye.Infrastructure.Application;

namespace SixthEye.Infrastructure.Pipeline
{
    [MessagePackObject]
    public class StageReporting
    { 
        [Key("id")]
        public Guid Id { get; set; } = Guid.NewGuid();

        [IgnoreMember]
        public int _flight = 0;
        [Key("flight")] 
        public int Flight { get => _flight; }
        
        [Key("processed")]
        public int Processed { get; set; } = 0;
        [Key("dropped")]
        public int Dropped { get; set; } = 0;
        [Key("fatal")]
        public int Fatal { get; set; } = 0;
        [Key("stall")]
        public int Stall { get; set; } = 0;
        [Key("peek")]
        public ConcurrentQueue<State> Peek { get; set; } = new ();

        public void AppendPeek(State state, bool dropped = false)
        {
            if(state is null)
                return;
            if (Peek.Count > 3)
                Peek.TryDequeue(out _); 
            Peek.Enqueue(state.Clone() as State);
        }
    }
}