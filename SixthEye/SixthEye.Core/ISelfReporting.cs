namespace SixthEye.Infrastructure.Pipeline
{
    public interface ISelfReporting
    {
        public object SelfReport();
        public StageReporting Counters(); 
    }
}