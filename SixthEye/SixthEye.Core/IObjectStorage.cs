using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace SixthEye.Infrastructure.ObjectStorage
{
    public interface IObjectStorage
    {
        public Task<Stream> Get(string objectName, string folder = null);

        public Task Put(Stream data, string objectName, string folder = null);

        public Task<List<string>> List(string folder = null);
        public Task Delete(string objectName, string folder = null);
    }
}