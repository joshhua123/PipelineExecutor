using System;
using System.Collections.Generic; 
using System.Linq; 
using MessagePack;

namespace SixthEye.Infrastructure.Application
{ 
    // State is a unit of work, state is fed through pipelines to transform their internal state
    // State may be persisted as unfinished work, artifacts of state should be saved within the warehouse
    [MessagePackObject]
    public sealed class State : ICloneable
    {
        [Key(0)]
        public string Id;
        [Key(1)]
        public IDictionary<string, string> variables { get; set; } = new Dictionary<string, string>();
        [IgnoreMemberAttribute]
        public bool Debug { get; set; }
        [IgnoreMemberAttribute]
        private List<IDictionary<string, string>> _snapshots = null;

        public State() {
            Id = Guid.NewGuid().ToString(); 
        } 

        public object Clone() {
            if (Debug) {
                _snapshots ??= new List<IDictionary<string, string>>();
                _snapshots.Add(new Dictionary<string, string>(variables));
            }

            var copy = new Dictionary<string, string>(variables);
            return new State() {
                variables = copy,
                _snapshots = (!Debug)
                    ? null
                    : new List<IDictionary<string, string>>(_snapshots?.Append(copy) 
                                                            ?? Array.Empty<IDictionary<string, string>>())
            };
        }
    }
}