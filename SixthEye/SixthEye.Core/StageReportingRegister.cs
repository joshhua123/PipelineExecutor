using System.Collections.Concurrent;
using System.Collections.Generic;

namespace SixthEye.Infrastructure.Pipeline
{
    public class StageReportingRegister
    {

        readonly ConcurrentDictionary<string, StageReporting> _lookup = new ();
        
        public void Register(StageReporting stage) {
            _lookup.TryAdd(stage.Id.ToString(), stage);
        }

        public StageReporting Get(string guid)
        {
            if (_lookup.TryGetValue(guid, out var value))
                return value;
            return null;
        }
    }
}