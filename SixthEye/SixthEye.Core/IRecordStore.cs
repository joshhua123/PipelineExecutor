using System.Collections.Generic;
using System.Threading.Tasks;
using SixthEye.Infrastructure.Application;
  
public interface IRecordStore
{
    public Task<IEnumerable<IDictionary<string, string>>> Get(string partition);
    public IEnumerable<IDictionary<string, string>> Iterate(string partition);
    public Task Put(State state, string partition, string[] selector);
    public Task<bool> Contains(string key, string value, string partition);
    /// <summary>
    /// List all partitions of the warehouse
    /// </summary>
    /// <returns></returns>
    public Task<IEnumerable<string>> List();
} 