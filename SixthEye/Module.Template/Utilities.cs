﻿using Newtonsoft.Json;
using PipelineService.Plugins;
using SixthEye.Infrastructure.Application;
using SixthEye.Infrastructure.Pipeline;

namespace Module.Template;

public class Utilities : IPlugin
{ 
        private readonly StageReporting _report = new(); 

        private string _on;
        private string _operation;
        private string _value;
        
        private enum FunctionEnum {
            Split,
            Flatten,
            Filter
        }

        private FunctionEnum _functionEnum;
        public async Task<State[]> Run(State state) {  
            return _functionEnum switch { 
                FunctionEnum.Split => await Split(state),
                FunctionEnum.Flatten => await Flatten(state),
                FunctionEnum.Filter => await Filter(state),
                _ => null
            };
        }
 
        public void Init(string function, IDictionary<string, string> parameters)
        {
            _functionEnum = function switch {
                "split" => FunctionEnum.Split,
                "flatten" => FunctionEnum.Flatten,
                "filter" => FunctionEnum.Filter,
                _ => _functionEnum
            };

            _ = _functionEnum switch {
                FunctionEnum.Split => IPlugin.ValidateParameters(new []{"on"}, parameters),
                FunctionEnum.Flatten => IPlugin.ValidateParameters(new []{"on"}, parameters),
                FunctionEnum.Filter => IPlugin.ValidateParameters(new []{"on", "operation", "value"}, parameters),
                _ => throw new ArgumentOutOfRangeException()
            };

            parameters.TryGetValue("on", out _on);
            parameters.TryGetValue("operation", out _operation);
            parameters.TryGetValue("value", out _value);
        }

        public IDictionary<string, IEnumerable<string>> Functions() {
            return new Dictionary<string, IEnumerable<string>>(){
                {"split", new [] {"on"}},
                {"flatten", new [] {"on"}},
                {"filter", new [] {"on", "operation", "value"}}
            };
        }

        private Task<State[]> Split(State state)
        {  
            if (!state.variables.TryGetValue(_on, out var splitVariables))
            {
                _report.Fatal++;
                _report.Processed++;
                return Task.FromResult(new[] { state });
            }
            //throw new Exception($"'on' parameter {splitSelector}, doesn't exist in state {state.Id}");

            var result = JsonConvert.DeserializeObject<object[]>(splitVariables).Select(x =>
            {
                var newState = state.Clone() as State;
                newState.variables[_on] = JsonConvert.SerializeObject(x); 
                _report.AppendPeek(newState);
                return newState;
            }).ToArray();
            
            _report.Processed++; 
    
            return Task.FromResult(result); 
        }
        
        private Task<State[]> Flatten(State state) {
            if (!state.variables.TryGetValue(_on, out var flattenVariables)) {
                _report.Dropped++;
                _report.AppendPeek(state, true); 
                return Task.FromResult(Array.Empty<State>());
            }

            foreach (var variable in JsonConvert.DeserializeObject<IDictionary<string, string>>(flattenVariables).Where(x => x.Value != null)) {
                state.variables.TryAdd(variable.Key, variable.Value);
            }

            _report.Processed++;
            _report.AppendPeek(state);
            
            return Task.FromResult(new [] {state});
        }
        
        private Task<State[]> Filter(State state) {
            if (!state.variables.TryGetValue(_on, out var stateParam))
            {
                _report.Dropped++;
                _report.AppendPeek(state, true);
                return Task.FromResult(Array.Empty<State>());
;            }
            
            _report.Processed++; 
            _report.AppendPeek(state);
            
            switch (_operation) {
                case "isEmpty":
                    var booleanValue = bool.Parse(_value);
                    return Task.FromResult(
                        string.IsNullOrWhiteSpace(stateParam) == booleanValue ? new [] {state} : Array.Empty<State>());
                case "equals":  
                    return Task.FromResult(_value.Equals(stateParam) ? new [] {state} : Array.Empty<State>());
                default:
                    return Task.FromResult(new [] {state});
            }
        }

        public object SelfReport()
        {
            return new { nameof = nameof(Utilities) };
        }

        public StageReporting Counters()
        {
            return _report;
        }

        public void Dispose()
        {
        }
}